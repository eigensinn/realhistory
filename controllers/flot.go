package controllers

import (
	"encoding/json"
	"github.com/astaxie/beego"
	"github.com/jinzhu/gorm"
    "github.com/deckarep/golang-set"
    "reflect"
    "strconv"
)

type FlotController struct {
	beego.Controller
}

type AppendToFlotController struct {
	beego.Controller
}

type EventChangeStruct struct {
	TblName string `json:"tbl_name"`
	Idx string `json:"idx"`
	DateTo int `json:"dateto"`
	Edge int `json:"edge"`
}

type ToStruct struct {
	DupIdx string
}

type From struct {
	SeriesIndex int `json:"seriesIndex"`
	DataIndex int `json:"dataIndex"`
	DataFieldX int `json:"dataFieldX"`
}

type To struct {
	SeriesIndex int `json:"seriesIndex"`
	DataIndex int `json:"dataIndex"`
	DataFieldX int `json:"dataFieldX"`
}

type LineData struct {
	From From `json:"from"`
	To To `json:"to"`
}

type ReceiveGraphStruct struct {
	TblName string
	EventType string
	Start string
	End string
}

type RowsStruct struct {
	Start int
	End int
	Idx string
	Name string
}

func (this *FlotController) Get() {
	s := GetSettings()
	db, err := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
	if err != nil {
		beego.Info(err)
	}
	this.Data["xsrf_token"] = this.XSRFToken()
	this.Data["Url"] = "127.0.0.1:8080"
	this.Data["active"] = "#lnk_flot"
	this.Layout = "base.tpl"
	this.LayoutSections = make(map[string]string)
	this.TplName = "flot/flot.tpl"
	this.LayoutSections["Head"] = "flot/flot_head.tpl"
	this.LayoutSections["Header"] = "header.tpl"
	this.LayoutSections["Nav"] = "nav.tpl"
	this.LayoutSections["Footer"] = "footer.tpl"
	session := this.GetSession("authorized")
	if session != nil {
		var e EmailStruct
		db.Table("users").Where("id = ?", session.(int)).Select("email").Scan(&e)
		this.Data["email"] = e.Email
		tbl_names, set := GetTableNamesAndEventTypes(session.(int))
		this.Data["tblNames"] = &tbl_names
		this.Data["eventTypes"] = &set
	}
}

func stringInSlice(a string, list []interface{}) bool {
    for _, b := range list {
        if b == a {
            return true
        }
    }
    return false
}

func Insert(slice []interface{}, idx int, value int) []interface{} {
    newSlice := make([]interface{}, len(slice)+1, len(slice)*2)
    copy(newSlice[:idx], slice[:idx])
    copy(newSlice[idx+1:], slice[idx:])
    newSlice[idx] = value
    return newSlice
}

func (this *AppendToFlotController) AppendToFlot() {
	//Получаем переданные имя таблицы и тип события
	var rgs ReceiveGraphStruct
	json.Unmarshal(this.Ctx.Input.RequestBody, &rgs)
	//Сканируем таблицу params по tbl_name
	var h HeadsStruct
	s := GetSettings()
	db, _ := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
	db.Table("params").Where("tbl_name = ? AND user_id = ?", rgs.TblName, this.GetSession("authorized").(int)).Select("id, heads, idx, name, description, start, \"end\", region, event_type, shift_date").Scan(&h)
	
	//Срез, который передается клиенту
	var rows_slice [][]interface{}
	
	StartI, _ := strconv.Atoi(rgs.Start)
	EndI, _ := strconv.Atoi(rgs.End)
	//Считываем в rows события типа eventtype, дата которых находится между start и end
	rows, err := db.Table(rgs.TblName).Where("\""+h.Start+"\""+" >= ? AND "+"\""+h.Start+"\""+" <= ? AND "+"\""+h.EventType+"\""+" = ?", StartI, EndI, rgs.EventType).Select("\""+h.Start+"\""+", "+"\""+h.End+"\""+", "+"\""+h.Idx+"\""+", "+"\""+h.Name+"\"").Rows()
	defer rows.Close()
	var rs RowsStruct
	if err == nil {
		for rows.Next() {
			var cols []interface{}
			rows.Scan(&rs.Start, &rs.End, &rs.Idx, &rs.Name)
			rs_obj := reflect.ValueOf(&rs).Elem()
			for i := 0; i < rs_obj.NumField(); i++ {
				cols = append(cols, rs_obj.Field(i).Interface())
			}
			rows_slice = append(rows_slice, cols)
		}
	}
	
	//Вставляем индексы i rows_slice[i] в сам rows_slice
	for i, value := range rows_slice {
		rows_slice[i] = Insert(value, 1, i)
	}
	
	//Определяем индекс таблицы для выборки из rel_events и duplicates
	event_par_id, _ := strconv.Atoi(h.Id)
	
	var rel_idxs []interface{}
	db.Table("rel_events").Where("event_par_id = ?", event_par_id).Pluck("rel_idx", &rel_idxs)
	ri_set := mapset.NewSetFromSlice(rel_idxs).ToSlice()
	
	//Контейнер связанных событий
	var rel_cont [][]interface{}
	var event_idxs_r []interface{}
	db.Table("rel_events").Where("event_par_id = ?", event_par_id).Pluck("event_idx", &event_idxs_r)
	ei_set := mapset.NewSetFromSlice(event_idxs_r).ToSlice()
	for _, value := range rows_slice {
		//Если Idx (value[3]) in event_idxs
		if stringInSlice(value[3].(string), ei_set) {
			var rel_slice []interface{}
			rel_slice = append(rel_slice, value)
			var ts ToStruct
			//Находим все события, связанные с value[3]
			rel_idxs, err := db.Table("rel_events").Where("event_idx = ? AND event_par_id = ? AND rel_par_id = ?", value[3].(string), event_par_id, event_par_id).Select("rel_idx").Rows()
			defer rel_idxs.Close()
			if err == nil {
				for rel_idxs.Next() {
					rel_idxs.Scan(&ts.DupIdx)
					for _, value := range rows_slice {
						//Находим номер строки rows_slice, которая содержит Idx == ts.DupIdx
						if value[3].(string) == ts.DupIdx {
							rel_slice = append(rel_slice, value)
						}
					}
				}
				if rel_slice != nil {
					rel_cont = append(rel_cont, rel_slice)
				}
			}
		} else {
			//Если события нет в обоих Pluck-контейнерах, добавляем его в rel_cont как [[event]]
			if stringInSlice(value[3].(string), ri_set) == false {
				var one_slice []interface{}
				one_slice = append(one_slice, value)
				rel_cont = append(rel_cont, one_slice)
			}
		}
	}
	
	//Контейнер дубликатов
	var linedata_cont [][]interface{}
	//Контейнер event_idxs из таблицы duplicates
	var event_idxs []interface{}
	db.Table("duplicates").Where("event_par_id = ?", event_par_id).Pluck("event_idx", &event_idxs)
	ei_set = mapset.NewSetFromSlice(event_idxs).ToSlice()
	for i, sl := range rel_cont {
		for j, value := range sl {
			//Если Idx (value[3]) in event_idxs
			if stringInSlice(value.([]interface{})[3].(string), ei_set) {
				var linedata_slice []interface{}
				var line_data LineData
				line_data.From = From{SeriesIndex: i, DataIndex: j, DataFieldX: 0}
				var ts ToStruct
				//Находим все дубликаты события value[3]
				dup_idxs, err := db.Table("duplicates").Where("event_idx = ? AND event_par_id = ? AND dup_par_id = ?", value.([]interface{})[3].(string), event_par_id, event_par_id).Select("dup_idx").Rows()
				defer dup_idxs.Close()
				if err == nil {
					for dup_idxs.Next() {
						dup_idxs.Scan(&ts.DupIdx)
						for k, sl2 := range rel_cont {
							for m, value := range sl2 {
								//Находим номер строки rows_slice, которая содержит Idx == ts.DupIdx
								if value.([]interface{})[3].(string) == ts.DupIdx {
									line_data.To = To{SeriesIndex: k, DataIndex: m, DataFieldX: 2}
									linedata_slice = append(linedata_slice, line_data)
								}
							}
						}
					}
					linedata_cont = append(linedata_cont, linedata_slice)
				}
			}
		}
	}
	
	var slice [][]interface{}
	slice = append(slice, linedata_cont...)
	slice = append(slice, rel_cont...)
	
	this.Data["json"] = &slice
	this.ServeJSON()
}

//Изменение даты события
func (this *AppendToFlotController) EventChange() {
	var ecs EventChangeStruct
	json.Unmarshal(this.Ctx.Input.RequestBody, &ecs)
	s := GetSettings()
	db, _ := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
	var h HeadsStruct
	user_id := this.GetSession("authorized").(int)
	//Сдвинуто начало
	if ecs.Edge == 1 {
		db.Table("params").Where("tbl_name = ? AND user_id = ?", ecs.TblName, user_id).Select("idx, start").Scan(&h)
		db.Table(ecs.TblName).Where("\""+h.Idx+"\""+" = ?", ecs.Idx).Update(h.Start, ecs.DateTo)
		//Сдвинуто окончание
	} else if ecs.Edge == 2 {
		db.Table("params").Where("tbl_name = ? AND user_id = ?", ecs.TblName, user_id).Select("idx, \"end\"").Scan(&h)
		db.Table(ecs.TblName).Where("\""+h.Idx+"\""+" = ?", ecs.Idx).Update(h.End, ecs.DateTo)
	}
	this.ServeJSON()
}