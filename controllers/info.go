package controllers

import (
	"github.com/astaxie/beego"
	"github.com/jinzhu/gorm"
    _ "github.com/jinzhu/gorm/dialects/postgres"
)

type InfoController struct {
	beego.Controller
}

type TechController struct {
	beego.Controller
}

func (this *InfoController) Get() {
	s := GetSettings()
	db, err := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
	if err != nil {
		beego.Info(err)
	}
	this.Data["Url"] = "127.0.0.1:8080"
	this.Data["xsrf_token"] = this.XSRFToken()
	this.Data["active"] = "#lnk_info"
	this.Layout = "base.tpl"
	this.LayoutSections = make(map[string]string)
	this.TplName = "info/info.tpl"
	this.LayoutSections["Head"] = "info/info_head.tpl"
	this.LayoutSections["Header"] = "header.tpl"
	this.LayoutSections["Nav"] = "nav.tpl"
	this.LayoutSections["Footer"] = "footer.tpl"
	session := this.GetSession("authorized")
	if session != nil {
		var e EmailStruct
		db.Table("users").Where("id = ?", session.(int)).Select("email").Scan(&e)
		this.Data["email"] = e.Email
	}
}

func (this *TechController) Get() {
	s := GetSettings()
	db, err := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
	if err != nil {
		beego.Info(err)
	}
	this.Data["Url"] = "127.0.0.1:8080"
	this.Data["xsrf_token"] = this.XSRFToken()
	this.Data["active"] = "#lnk_tech"
	this.Layout = "base.tpl"
	this.LayoutSections = make(map[string]string)
	this.TplName = "info/tech.tpl"
	this.LayoutSections["Head"] = "info/tech_head.tpl"
	this.LayoutSections["Header"] = "header.tpl"
	this.LayoutSections["Nav"] = "nav.tpl"
	this.LayoutSections["Footer"] = "footer.tpl"
	session := this.GetSession("authorized")
	if session != nil {
		var e EmailStruct
		db.Table("users").Where("id = ?", session.(int)).Select("email").Scan(&e)
		this.Data["email"] = e.Email
	}
}