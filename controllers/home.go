package controllers

import (
	"github.com/astaxie/beego"
	"github.com/jinzhu/gorm"
)

type MainController struct {
	beego.Controller
}

type TableStruct struct {
	TblName string
	RelParId int
	RelIdx string
}

func createTable(db *gorm.DB, tbl_name string, par_id string, idx string) {
	//Проверяем, существует ли таблица
	if db.HasTable(tbl_name) == false {
		//Создаем таблицу с ограничением по наличию записей в таблице Param с id == param_id
		db.Exec("CREATE TABLE "+tbl_name+" (id integer PRIMARY KEY, event_par_id integer REFERENCES params, event_idx text, "+par_id+" integer REFERENCES params, "+idx+" text);")
	}
}

func (this *MainController) Get() {
	this.Layout = "base.tpl"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["Head"] = "home/home_head.tpl"
	this.LayoutSections["Header"] = "header.tpl"
	this.LayoutSections["Nav"] = "nav.tpl"
	this.LayoutSections["Footer"] = "footer.tpl"
	this.TplName = "home/home.tpl"
	this.Data["xsrf_token"] = this.XSRFToken()
	this.Data["Url"] = "127.0.0.1:8080"
	this.Data["active"] = "#lnk_home"
	session := this.GetSession("authorized")
	if session != nil {
		s := GetSettings()
		db, err := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
		if err != nil {
			beego.Info(err)
		}
		tbl_names := [3]string{ "rel_events", "duplicates", "equals" }
		for _, value := range tbl_names {
			switch value {
				case "rel_events":
					createTable(db, "rel_events", "rel_par_id", "rel_idx")
				case "duplicates":
					createTable(db, "duplicates", "dup_par_id", "dup_idx")
				case "equals":
					createTable(db, "equals", "eq_par_id", "eq_idx")
			}
		}
		var e EmailStruct
		db.Table("users").Where("id = ?", session.(int)).Select("email").Scan(&e)
		this.Data["email"] = e.Email
	}
}