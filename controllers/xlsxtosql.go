package controllers

import (
	"encoding/json"
	"github.com/astaxie/beego"
	"github.com/saintfish/chardet"
	"github.com/tealeg/xlsx"
	"github.com/jinzhu/gorm"
	"golang.org/x/text/encoding/charmap"
	"golang.org/x/text/transform"
	"io/ioutil"
	"path/filepath"
	"reflect"
	"strings"
	"strconv"
	"os"
    _ "github.com/jinzhu/gorm/dialects/postgres"
    "github.com/jmoiron/sqlx"
    models "realhistory/models"
)

type XLSXController struct {
	beego.Controller
}

type XLSXtoSQLController struct {
	beego.Controller
}

type FillController struct {
	beego.Controller
}

type ActionController struct {
	beego.Controller
}

type ActStruct struct {
	Tbl string `json:"rh_tbl"`
	Col string `json:"rh_col"`
    Row string `json:"rh_row"`
}

type ActSaveStruct struct {
	Params *ActStruct `json:"params"`
	NewData map[string]interface{} `json:"new_data"`
}

type HeadsStruct struct {
	Id string
	TblName string
	Heads string
	Idx string
	Name string
	Description string
	Start string
	End string
	Region string
	EventType string
	ShiftDate string
}

type EmailStruct struct {
	Email string
}

func (this *XLSXController) Get() {
	s := GetSettings()
	db, err := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
	if err != nil {
		beego.Info(err)
	}
	//Считываем из БД имена таблиц
	var tbl_names []string
	db.AutoMigrate(&models.Param{})
	db.Model(&models.Param{}).Where("user_id = ?", this.GetSession("authorized").(int)).Pluck("tbl_name", &tbl_names)
	this.Data["tblNames"] = &tbl_names
	this.Data["Url"] = "127.0.0.1:8080"
	this.Data["xsrf_token"] = this.XSRFToken()
	this.Data["active"] = "#lnk_xlsx"
	this.Layout = "base.tpl"
	this.LayoutSections = make(map[string]string)
	this.TplName = "xlsx/xlsx.tpl"
	this.LayoutSections["Head"] = "xlsx/xlsx_head.tpl"
	this.LayoutSections["Header"] = "header.tpl"
	this.LayoutSections["Nav"] = "nav.tpl"
	this.LayoutSections["Footer"] = "footer.tpl"
	session := this.GetSession("authorized")
	if session != nil {
		var e EmailStruct
		db.Table("users").Where("id = ?", session.(int)).Select("email").Scan(&e)
		this.Data["email"] = e.Email
	}
}

//Получение заголовков и соответствий столбцов при выборе имени таблицы
func (this *FillController) Heads() {
	tbl_name := this.GetString("table")
	var h HeadsStruct
	s := GetSettings()
	db, _ := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
	db.Table("params").Where("tbl_name = ? AND user_id = ?", tbl_name, this.GetSession("authorized").(int)).Select("heads, idx, name, description, start, \"end\", region, event_type, shift_date").Scan(&h)
	this.Data["json"] = &h
    this.ServeJSON()
}

func (this *FillController) Fill() {
	//Получаем имя таблицы
	tbl_name := this.GetString("table")
	var dbx *sqlx.DB
	s := GetSettings()
	dbx, _ = sqlx.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
	//Срез, который передается клиенту
	var rows_slice []map[string]interface{}
	//Пробуем подключиться к БД
	err := dbx.Ping()
	if err == nil {
		//Считываем всю таблицу в rows
		rows, err := dbx.Queryx("SELECT * FROM "+"\""+tbl_name+"\"")
		if err == nil {
			for rows.Next() {
				//Определяем карту, в которую будут передаваться значения
			    rows_map := make(map[string]interface{})
			    err = rows.MapScan(rows_map)
			    if err == nil {
			    	//Конвертируем байты из карты в строки
					for key, value := range rows_map {
						switch value := value.(type) {
							case []uint8:
								rows_map[key] = string(value[:])
						}	
					}
					//Добавляем карту в срез
			    	rows_slice = append(rows_slice, rows_map)
			    }
			}
			this.Data["json"] = &rows_slice
		} else {
			beego.Info(err)
		}
	}
    this.ServeJSON()
}

func (this *XLSXtoSQLController) XLSXtoSQL() {
	//Создаем структуру для параметров
	var ps models.Param
	
	//Получаем файл file-0 и сохранем его в dummy/root_storage
	file, header, err := this.GetFile("file-0")
	if err != nil {
		beego.Info(err)
	} else {
		if file != nil {
	        // get the filename
	        ps.Path = generateSalt() + "_" + header.Filename
	        // save to server
	        err := this.SaveToFile("file-0", "./dummy/root_storage/"+ps.Path)
	        if err != nil {
				beego.Info(err)
			}
	    }
	}
	
	//Получаем col_names и data_row из файла file-1 в оперативной памяти
	cn, header, err := this.GetFile("file-1")
	st := make([]string, 2)
	if err != nil {
		beego.Info(err)
	} else {
		b1 := make([]byte, 3)
		cn.Read(b1)
		st = strings.Split(string(b1), "_")
	}
	
	//Создаем файл базы данных
	s := GetSettings()
	db, _ := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
	//Определяем номер строки с именами столбцов
	col_names, _ := strconv.Atoi(st[0])
	//Определяем номер первой строки с данными
	data_row, _ := strconv.Atoi(st[1])
	tbl := make(map[int]map[string]interface{})
	excelFileName, _ := filepath.Abs("./dummy/root_storage/"+ps.Path)
    xlFile, err := xlsx.OpenFile(excelFileName)
    if err != nil {
        this.Data["json"] = err
    } else {
    	//Соединяем RAW-строку и создаем таблицу с именем, получаемым из ps.Path
	    ps.TblName = strings.Replace(strings.Split(ps.Path, ".")[0], "/", "_", -1)
	    //Создаем срез с именами столбцов и заполняем его
	    heads := []string{}
	    for col_idx, _ := range xlFile.Sheets[0].Cols {
	    	head, _ := xlFile.Sheets[0].Rows[col_names-1].Cells[col_idx].String()
	    	heads = append(heads, head)
	    }
	    //Сохраняем имена столбцов в виде строки
	    ps.Heads = strings.Join(heads, ",")
	    ps.UserID = this.GetSession("authorized").(int)
    	//Создаем или видоизменяем таблицу params
    	db.AutoMigrate(&models.Param{})
    	//Добавляем новую запись в params
    	db.Create(&ps)
    	for _, sheet := range xlFile.Sheets {
	        for row_idx, row := range sheet.Rows {
	        	//Инициализация вложенных карт - издержки языка Go
	        	if _, ok := tbl[row_idx]; !ok {
			        tbl[row_idx] = make(map[string]interface{})
			    }
	            for col_idx, cell := range row.Cells {
	            	cellType := cell.Type()
	            	//Определяем имя текущего столбца
	            	head, _ := sheet.Rows[col_names-1].Cells[col_idx].String()
	            	switch {
						//Если тип ячейки CellTypeString
						case cellType == 0:
							value, _ := cell.String()
			            	//Определяем кодировку
			            	detector := chardet.NewTextDetector()
							result, err := detector.DetectBest([]byte(value))
							if err == nil {
								if result.Charset == "ISO-8859-1" || result.Charset == "ISO-8859-9" {
									//Меняем кодировку
									r := transform.NewReader(strings.NewReader(value), charmap.Windows1251.NewEncoder())
					        		buf, err := ioutil.ReadAll(r)
					        		if err == nil {
					        			tbl[row_idx][head] = string(buf)
					        		}
								} else {
					        		tbl[row_idx][head] = value
								}
							}
						//Если тип ячейки CellTypeNumeric	
						case cellType == 2:
							if col_idx == 0 {
								value, _ := cell.Int()
								tbl[row_idx][head] = value
							} else {
								value, _ := cell.Float()
								tbl[row_idx][head] = value
							}
	            		//Если тип ячейки CellTypeBool
						case cellType == 3:
	            			value := cell.Bool()
					        tbl[row_idx][head] = value
	            	}
	            }
	        }
	    }
	    //Создаем словарь типов GoLang/SQL
	    types := map[string]string{"string":"text", "int":"integer PRIMARY KEY", "float64":"real", "bool":"boolean"}
	    //Создаем срез для формирования RAW-строки
	    raw := []string{}
	    for col_idx, _ := range xlFile.Sheets[0].Cols {
	    	//Заполняем срез для RAW-строки именами столбцов и их типами (как string)
	    	raw = append(raw, "\""+tbl[1][heads[col_idx]].(string)+"\"" + " " + types[reflect.TypeOf(tbl[2][heads[col_idx]]).String()])
	    }
	    st := "CREATE TABLE "+"\""+ps.TblName+"\""+" ("+strings.Join(raw, ", ")+");"
	    db.Exec(st)
	    //Создаем все записи таблицы SQL (заполняем первый столбец)
	    for row_idx, row := range tbl {
	    	//Пропускаем первые строки с заголовками таблицы и столбцов
	    	if row_idx >= data_row-1 {
	    		if row[heads[1]] != "" {
			    	db.Exec("INSERT INTO "+"\""+ps.TblName+"\""+" ("+heads[0]+") VALUES(?);", row[heads[0]])
			    	//Тут должно быть нечто, передающее в браузер ход процесса
	    		}
	    	}
	    }
	    //Обновляем  таблицы SQL
	    for row_idx, row := range tbl {
	    	if row_idx >= data_row-1 {
	    		if row[heads[1]] != "" {
	    			//Обновляем записи в базе данных, ориентируясь на первый столбец
			    	db.Table(ps.TblName).Where("id = ?", row_idx-data_row+1).Updates(row)
	    		}
	    	}
	    }
	    //Удаляем исходный файл после конвертации
	    err := os.Remove("./dummy/root_storage/"+ps.Path)
	    if err != nil {
	    	beego.Info(err)
	    }
	    this.Data["json"] = ps.TblName
    }
    this.ServeJSON()
}

//Удаляет строку в таблице
func (this *ActionController) RemoveRowAct() {
	var as ActStruct
	s := GetSettings()
	db, _ := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
    json.Unmarshal(this.Ctx.Input.RequestBody, &as)
	db.Table(as.Tbl).Where(""+as.Col+" = ?", as.Row).Delete(as.Row)
	this.Data["json"] = "Строка "+as.Row+" удалена"
    this.ServeJSON()
}

func CheckFields(fields []string, as *ActStruct) bool {
	find := false
	for _, row := range fields {
		if row == as.Row {
			find = true
		}
	}
	return find
}

//Обновление данных
func (this *ActionController) SaveAct() {
	var ass ActSaveStruct
	json.Unmarshal(this.Ctx.Input.RequestBody, &ass)
	as := ass.Params
	s := GetSettings()
	db, _ := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
	var fields []string
	db.Table(as.Tbl).Pluck(as.Col, &fields)
	//Определяем, содержится ли уже переданное значение
	if CheckFields(fields, as) {
		db.Table(as.Tbl).Where(""+as.Col+" = ?", as.Row).Updates(ass.NewData)
		this.Data["json"] = "Строка "+as.Row+" обновлена"
	} else {
		db.Exec("INSERT INTO "+"\""+as.Tbl+"\""+" ("+as.Col+") VALUES(?);", as.Row)
		db.Table(as.Tbl).Where(""+as.Col+" = ?", as.Row).Updates(ass.NewData)
		this.Data["json"] = "Создана новая строка "+as.Row
	}
    this.ServeJSON()
}

//Установка соответствия столбцов
func (this *ActionController) SendCol() {
	var ps models.Param
	json.Unmarshal(this.Ctx.Input.RequestBody, &ps)
	s := GetSettings()
	db, _ := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
	db.AutoMigrate(&models.Param{})
	db.Model(&ps).Where("tbl_name = ?", ps.TblName).Updates(&ps)
	this.Data["json"] = "Таблица соответствий обновлена"
    this.ServeJSON()
}