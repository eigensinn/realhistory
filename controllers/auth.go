package controllers

import (
	"encoding/json"
	"github.com/astaxie/beego"
	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
	"math/rand"
	"time"
	"strings"
	"net/smtp"
	"encoding/base64"
	"encoding/gob"
	"os"
    models "realhistory/models"
)

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

const (
	SaltLength = 64
	// On a scale of 3 - 31, how intense Bcrypt should be
	EncryptCost = 14
)

type Pair struct {
	Email string `json:"email"`
	Password string `json:"password"`
}

type IdStruct struct {
	Id int
}

type Response struct {
	Text string `json:"text"`
	Code int `json:"code"`
}

type AuthController struct {
	beego.Controller
}

type CoordController struct {
	beego.Controller
}

type SettingsController struct {
	beego.Controller
}

func reverse(array []byte) []byte {
	newArray := make([]byte, len(array))
	for i, j := 0, len(array)-1; i < j; i, j = i+1, j-1 {
		newArray[i], newArray[j] = array[j], array[i]
	}
	return newArray
}

func GetSettings() models.Settings {
	var s models.Settings
	file, err := os.Open("service.set")
	if err != nil {
 		beego.Info(err)
 	}
	dec := gob.NewDecoder(file)
 	err = dec.Decode(&s)
 	if err != nil {
 		beego.Info(err)
 	}
 	file.Close()
 	var buf []byte
 	buf, err = base64.StdEncoding.DecodeString(string(s.DbPwdByte))
 	if err != nil {
 		beego.Info(err)
 	}
 	var buf2 []byte
 	buf2, err = base64.StdEncoding.DecodeString(string(reverse(buf)))
 	s.DbPwd = string(buf2)
 	return s
}

func Mail(db *gorm.DB, user *models.User) {
	user.Confirm = generateSalt()
	// Set up authentication information.
    auth := smtp.PlainAuth(
        "",
        "site@mail.com",
        "mailpwd777",
        "smtp.mail.com",
    )
    
    mime := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n";
	subject := "Subject: realhistory\n"
	msg := []byte(subject + mime + "<html><body><p>Для подтверждения email пройдите по <a href='http://127.0.0.1:8080/confirm/"+user.Confirm+"'>ссылке</a></p></body></html>")
    
    // Connect to the server, authenticate, set the sender and recipient,
    // and send the email all in one step.
    err1 := smtp.SendMail(
        "smtp.mail.com:777",
        auth,
        "site@mail.com",
        []string{user.Email},
        msg,
    )
    if err1 != nil {
        beego.Info(err1)
    } else {
    	db.Create(&user)
    	//Создаем таблицы last_activities и params, связанные по ограничению внешнего ключа с users
    	db.AutoMigrate(&models.LastActivity{}, &models.Param{}, &models.Coord{})
		db.Model(&models.LastActivity{}).AddForeignKey("user_id", "users(id)", "RESTRICT", "RESTRICT")
		db.Model(&models.Param{}).AddForeignKey("user_id", "users(id)", "RESTRICT", "RESTRICT")
		db.Model(&models.Coord{}).AddForeignKey("user_id", "users(id)", "RESTRICT", "RESTRICT")
    }
}

// Handles merging together the salt and the password
func combine(salt string, raw_pass string) string {
	// concat salt + password
	pieces := []string{salt, raw_pass}
	salted_password := strings.Join(pieces, "")
	return salted_password
}

//Generates a random salt using DevNull
func generateSalt() string {
	rand.Seed(time.Now().UnixNano())
    b := make([]rune, SaltLength)
    for i := range b {
        b[i] = letters[rand.Intn(len(letters))]
    }
    return string(b)
}

func (this *AuthController) CreateUser() {
	// Get the form values out of the POST request
	var pair Pair
	json.Unmarshal(this.Ctx.Input.RequestBody, &pair)
	var user models.User
	user.Salt = generateSalt()
	salted_pass := combine(user.Salt, pair.Password)
	// Generate a hashed password from bcrypt.
  	hashedPass, err := bcrypt.GenerateFromPassword([]byte(salted_pass), EncryptCost)
	if err != nil {
		beego.Info(err)
	}
	s := GetSettings()
	db, _ := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
	
	db.AutoMigrate(&user)
	db.Where(models.User{Email: pair.Email}).First(&user).Scan(&user)
	if user.Email == "" {
		user.Email = pair.Email
		user.Password = hashedPass
		// Stick that in our users table of our db.
		Mail(db, &user)
		this.Data["json"] = Response{Text: "Код активации отправлен на Email "+user.Email, Code: 1}
	} else {
		this.Data["json"] = Response{Text: "Email "+user.Email+" уже зарегистрирован в системе", Code: 0}
	}
	this.ServeJSON()
}

func (this *AuthController) Auth() {
	s := GetSettings()
 	db, err := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
 	if err != nil {
 		beego.Info(err)
 	}
	//db, _ := gorm.Open("postgres", "host=127.0.0.1 user=gorm dbname=gorm sslmode=disable password=rangemurata")
	session := this.GetSession("authorized")
	if session != nil {
		var e EmailStruct
		db.Table("users").Where("id = ?", session.(int)).Select("email").Scan(&e)
		this.Data["json"] = Response{Text: "Вы уже авторизованы как "+e.Email+" – надо выйти ", Code: 0}
	} else {
		// Get the form values out of the POST request.
	  	var pair Pair
		json.Unmarshal(this.Ctx.Input.RequestBody, &pair)
	
	  	// Find the user by his name and get the password_digest we generated in the create method out.
	  	var user models.User
		db.Where(models.User{Email: pair.Email}).First(&user).Scan(&user)
		if user.Email == "" {
			this.Data["json"] = Response{Text: "Email "+pair.Email+" не зарегистрирован в системе", Code: 0}
		} else {
			if user.Activated == true {
				salted_guess := combine(user.Salt, pair.Password)
				// Compare that password_digest to our password we got from the form value.
				// If the error is not equal to nil, we know the auth failed. If there is no error, it
				// was successful.
			  	if err := bcrypt.CompareHashAndPassword(user.Password, []byte(salted_guess)); err != nil {
			    	this.Data["json"] = Response{Text: "Неверный пароль", Code: 0}
			  	} else {
			  		var user_id IdStruct
			  		db.Table("users").Where("email = ?", user.Email).Select("id").Scan(&user_id)
				    this.SetSession("authorized", user_id.Id)
			    	this.Data["json"] = Response{Text: "Вы вошли под именем "+pair.Email, Code: 1}
			  	}
			} else {
				this.Data["json"] = Response{Text: "Пожалуйста, подтвердите свой аккаунт", Code: 0}
			}
			
		}
	}
	this.ServeJSON()
}

func (this *AuthController) Confirm() {
	var user models.User
	word := this.Ctx.Input.Param(":word")
	s := GetSettings()
	db, _ := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
	db.Where(models.User{Confirm: word}).First(&user).Scan(&user)
	if user.Confirm == word {
		user.Activated = true
		user.Confirm = ""
		db.Save(&user)
		this.Data["confirm"] = "Почта подтверждена, аккаунт "+user.Email+" активирован"
	}
	this.Layout = "base.tpl"
	this.LayoutSections = make(map[string]string)
	this.LayoutSections["homeHead"] = "home/home_head.tpl"
	this.LayoutSections["Header"] = "header.tpl"
	this.LayoutSections["Nav"] = "nav.tpl"
	this.LayoutSections["Footer"] = "footer.tpl"
	this.TplName = "home/home.tpl"
	this.Data["xsrf_token"] = this.XSRFToken()
	this.Data["Url"] = "127.0.0.1:8080"
	this.Data["active"] = "#lnk_home"
}

func (this *AuthController) Logout() {
	session := this.GetSession("authorized")
	if session != nil {
		this.DelSession("authorized")
		this.Redirect("/", 302)
		return
	}
}

func (this *CoordController) SaveCoord() {
	var coord models.Coord
	json.Unmarshal(this.Ctx.Input.RequestBody, &coord)
	session := this.GetSession("authorized")
	if session != nil {
		coord.UserID = session.(int)
		s := GetSettings()
		db, _ := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
		db.AutoMigrate(&models.Coord{})
		//db.Model(&models.Coord{}).AddForeignKey("user_id", "users(id)", "RESTRICT", "RESTRICT")
		var count int
		db.Model(&coord).Where("page = ? AND user_id = ?", coord.Page, coord.UserID).Count(&count)
		if count == 1 {
			if coord.Top == 0 && coord.Left == 0 {
				db.Model(&coord).Where("page = ? AND user_id = ?", coord.Page, coord.UserID).Update("maximized", coord.Maximized)
			} else {
				db.Model(&coord).Where("page = ? AND user_id = ?", coord.Page, coord.UserID).Update(&coord)
			}
		} else {
			db.Create(&coord)
		}
	}
	this.ServeJSON()
}

func (this *CoordController) GetCoord() {
	var coord models.Coord
	json.Unmarshal(this.Ctx.Input.RequestBody, &coord)
	session := this.GetSession("authorized")
	if session != nil {
		coord.UserID = session.(int)
		s := GetSettings()
		db, _ := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
		db.AutoMigrate(&models.Coord{})
		db.Model(&coord).Where("page = ? AND user_id = ?", coord.Page, coord.UserID).First(&coord)
		this.Data["json"] = &coord
	}
	this.ServeJSON()
}

func (this *SettingsController) SetSettings() {
	var s models.Settings
	json.Unmarshal(this.Ctx.Input.RequestBody, &s)
	db, err := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
	if err != nil {
		beego.Info(err)
		this.Data["json"] = "Неправильные реквизиты для подключения к БД"
	} else {
		db.AutoMigrate(&models.Settings{})
 		
 		file, err := os.Create("service.set")
		if err != nil {
	 		beego.Info(err)
	 		this.Data["json"] = "Не могу создать файл"
	 	} else {
	 		s.DbPwdByte = []byte(base64.StdEncoding.EncodeToString(reverse([]byte(base64.StdEncoding.EncodeToString([]byte(s.DbPwd))))))
			s.DbPwd = "None"
			enc := gob.NewEncoder(file)
	 		enc.Encode(s)
			this.Data["json"] = "Настройки обновлены"
	 	}
	 	file.Close()
	}
	this.ServeJSON()
}