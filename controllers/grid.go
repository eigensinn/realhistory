package controllers

import (
	"encoding/json"
	"github.com/astaxie/beego"
	"github.com/jinzhu/gorm"
    "github.com/jmoiron/sqlx"
    "github.com/deckarep/golang-set"
    "reflect"
    models "realhistory/models"
)

type GridController struct {
	beego.Controller
}

type AppendToGridController struct {
	beego.Controller
}

func GetTableNamesAndEventTypes(user_id int) ([]string, []interface{}){
	var h HeadsStruct
	s := GetSettings()
	db, _ := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
	var tbl_names []string
	//Создаем срез для всех типов событий
	var event_types []interface{}
	db.AutoMigrate(&models.Param{})
	//Получаем все имена таблиц
	db.Model(&models.Param{}).Where("user_id = ?", user_id).Pluck("tbl_name", &tbl_names)
	for _, value := range tbl_names {
		//Создаем срез для типов событий одной таблицы
		var event_type []interface{}
		//Для каждой таблицы получаем имя поля event_type
		db.Table("params").Where("tbl_name = ? AND user_id = ?", value, user_id).Select("event_type").Scan(&h)
		//Получаем типы событий по имени поля h.EventType для таблицы value
		db.Table(value).Pluck("\""+h.EventType+"\"", &event_type)
		//Добавляем типы событий в срез event_types
		event_types = append(event_types, event_type...)
	}
	//Преобразуем объекты uint8 (если они есть) в строки (подготовка к созданию множества)
	for i, value := range event_types {
		switch value := value.(type) {
			case []uint8:
				event_types[i] = string(value[:])
		}
	}
	//Создаем множество из среза
	set := mapset.NewSetFromSlice(event_types).ToSlice()
	return tbl_names, set
}

func (this *GridController) Get() {
	s := GetSettings()
	db, err := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
	if err != nil {
		beego.Info(err)
	}
	this.Data["xsrf_token"] = this.XSRFToken()
	this.Data["Url"] = "127.0.0.1:8080"
	this.Data["active"] = "#lnk_grid"
	this.Layout = "base.tpl"
	this.LayoutSections = make(map[string]string)
	this.TplName = "grid/grid.tpl"
	this.LayoutSections["Head"] = "grid/grid_head.tpl"
	this.LayoutSections["Header"] = "header.tpl"
	this.LayoutSections["Nav"] = "nav.tpl"
	this.LayoutSections["Footer"] = "footer.tpl"
	session := this.GetSession("authorized")
	if session != nil {
		var e EmailStruct
		db.Table("users").Where("id = ?", session.(int)).Select("email").Scan(&e)
		this.Data["email"] = e.Email
		tbl_names, set := GetTableNamesAndEventTypes(session.(int))
		this.Data["tblNames"] = &tbl_names
		this.Data["eventTypes"] = &set
	}
}

type ReceiveGridStruct struct {
	TblName string `json:"tbl_name"`
	EventType string `json:"event_type"`
}

type RemoveStruct struct {
	TblName string
	Idx string
}

type RelEvents struct {
	EventIdx string
	RelParId int
	RelIdx string
}

//Структура для передачи в GetRefs имен и ключей
type Names struct {
	TblName string
	XParId string
	XIdx string
	KeyParId string
	KeyIdx string
}

//Структура для операций по добавлению связанного события или дубликата
type Msg struct {
	XParId string
	XIdx string
	Msg1 string
	Msg2 string
}

//Возвращает максимальный Id+1
func BiggestId(list []int) int {
	if len(list) != 0 {
		max := list[0]
		for _, value := range list {
			if value > max {
				max = value
			}
		}
		return max + 1
	}
	return 0
}

//Функция добавления в rows_slice связаных событий и исторических дубликатов
func GetRefs(dbx *sqlx.DB, mq map[string]interface{}, s *RelEvents, rows_slice []map[string]interface{}, n *Names) []map[string]interface{} {
	rows, err := dbx.NamedQuery("SELECT event_idx, "+n.XParId+", "+n.XIdx+" FROM "+"\""+n.TblName+"\""+" WHERE event_par_id = :id AND event_idx = :idx", mq)
	if err == nil {
		for rows.Next() {
			rows_map := make(map[string]interface{})
		    err = rows.Scan(&s.EventIdx, &s.RelParId, &s.RelIdx)
		    if err == nil {
				rows_map["EventIdx"] = s.EventIdx
				rows_map[n.KeyParId] = s.RelParId
				rows_map[n.KeyIdx] = s.RelIdx
				rows_slice = append(rows_slice, rows_map)
		    }
		}
	} else {
		beego.Info(err)
	}
	return rows_slice
}

//Получаем данные для добавления в grid
func (this *AppendToGridController) AppendToGrid() {
	//Получаем переданные имя таблицы и тип события
	var rgs ReceiveGridStruct
	json.Unmarshal(this.Ctx.Input.RequestBody, &rgs)
	//Сканируем таблицу params по tbl_name
	var h HeadsStruct
	var dbx *sqlx.DB
	s := GetSettings()
	dbx, _ = sqlx.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
	//Пробуем подключиться к БД
	err := dbx.Ping()
	if err == nil {
		m := map[string]interface{}{"tbl_name": rgs.TblName, "user_id": this.GetSession("authorized").(int)}
		rows, err := dbx.NamedQuery(`SELECT id, heads, idx, name, description, start, "end", region, event_type, shift_date FROM params WHERE tbl_name= :tbl_name AND user_id= :user_id`, m)
		for rows.Next() {
		    err = rows.Scan(&h.Id, &h.Heads, &h.Idx, &h.Name, &h.Description, &h.Start, &h.End, &h.Region, &h.EventType, &h.ShiftDate)
		}
		if err == nil {
			//Срез, который передается клиенту
			var rows_slice []map[string]interface{}
			heads_map := make(map[string]string)
			//Создаем reflection структуры h
			h_obj := reflect.ValueOf(&h).Elem()
			//Производим итерацию по полям структуры h, игнорируя Id int
			for i := 1; i < h_obj.NumField(); i++ {
				//Получаем значение поля Получаем тип поля
				heads_map[h_obj.Field(i).Interface().(string)] = h_obj.Type().Field(i).Name
			}
			//Считываем в rows строки, содержащие переданный тип события EventType
			var rows *sqlx.Rows
			var err error
			if rgs.EventType == "*" {
				rows, err = dbx.Queryx("SELECT * FROM "+"\""+rgs.TblName+"\"")
			} else {
				rows, err = dbx.NamedQuery("SELECT * FROM "+"\""+rgs.TblName+"\""+" WHERE "+"\""+h.EventType+"\""+" = :eventtype", rgs)
			}
			if err != nil {
				beego.Info(err)
			}
			for rows.Next() {
				//Определяем карту, в которую будут передаваться значения
			    rows_map := make(map[string]interface{})
			    err = rows.MapScan(rows_map)
			    if err == nil {
			    	//Конвертируем байты из карты в строки
					for key, value := range rows_map {
						switch value := value.(type) {
							case []uint8:
								rows_map[key] = string(value[:])
						}
						rows_map[heads_map[key]] = rows_map[key]
					}
					rows_map["HId"] = h.Id
					//Определяем структуру для добавления связанного события
					var re RelEvents
					n := Names{TblName:"rel_events", XParId:"rel_par_id", XIdx: "rel_idx", KeyParId: "RelParId", KeyIdx: "RelIdx"}
					//Определяем карту для передачи в dbx.NamedQuery
					mq := map[string]interface{}{"id": h.Id, "idx": rows_map["Idx"]}
					rows_slice = GetRefs(dbx, mq, &re, rows_slice, &n)
					
					var d RelEvents
					n = Names{TblName:"duplicates", XParId:"dup_par_id", XIdx: "dup_idx", KeyParId: "DupParId", KeyIdx: "DupIdx"}
					rows_slice = GetRefs(dbx, mq, &d, rows_slice, &n)
					
					var eq RelEvents
					n = Names{TblName:"equals", XParId:"eq_par_id", XIdx: "eq_idx", KeyParId: "EqParId", KeyIdx: "EqIdx"}
					rows_slice = GetRefs(dbx, mq, &eq, rows_slice, &n)
					
					//Добавляем карты в срез
			    	rows_slice = append(rows_slice, rows_map)
			    }
			}
			this.Data["json"] = &rows_slice
		} else {
			beego.Info(err)
		}
	}
    this.ServeJSON()
}

//Удаление события, связанного события или дубликата
func (this *AppendToGridController) EventRemove() {
	var rrs models.LastActivity
	json.Unmarshal(this.Ctx.Input.RequestBody, &rrs)
	s := GetSettings()
	db, _ := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
	switch rrs.TblName {
		case "events":
			var rs RemoveStruct
			db.Table("params").Where("id = ? AND user_id = ?", rrs.XParId, this.GetSession("authorized").(int)).Select("tbl_name, idx").Scan(&rs)
			db.Table(rs.TblName).Where("\""+rs.Idx+"\""+" = ?", rrs.XIdx).Delete(rrs.XIdx)
			this.Data["json"] = "Событие "+rrs.XIdx+" удалено"
		case "rel_events":
			db.Table("rel_events").Where("event_idx = ? AND rel_par_id = ? AND rel_idx = ?", rrs.EventIdx, rrs.XParId, rrs.XIdx).Delete(&rrs.XIdx)
			this.Data["json"] = "Связанное событие "+rrs.XIdx+" удалено"
		case "duplicates":
			db.Table("duplicates").Where("event_idx = ? AND dup_par_id = ? AND dup_idx = ?", rrs.EventIdx, rrs.XParId, rrs.XIdx).Delete(&rrs.XIdx)
			this.Data["json"] = "Дубликат "+rrs.XIdx+" удален"
		case "equals":
			db.Table("equals").Where("event_idx = ? AND eq_par_id = ? AND eq_idx = ?", rrs.EventIdx, rrs.XParId, rrs.XIdx).Delete(&rrs.XIdx)
			this.Data["json"] = "Аналог "+rrs.XIdx+" удален"
	}
	this.ServeJSON()
}

func PluckAndInsert(db *gorm.DB, ris *models.LastActivity, msg *Msg) {
	var ids []int
	db.Table(ris.TblName).Pluck("id", &ids)
	db.Exec("INSERT INTO "+ris.TblName+" (id, event_par_id, event_idx, "+msg.XParId+", "+msg.XIdx+") VALUES (?,?,?,?,?);", BiggestId(ids), &ris.EventParId, &ris.EventIdx, &ris.XParId, &ris.XIdx)
}

//Добавление связанного события или дубликата
func (this *AppendToGridController) RelInsert() {
	var ris models.LastActivity
	json.Unmarshal(this.Ctx.Input.RequestBody, &ris)
	s := GetSettings()
	db, _ := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
	var msg Msg
	switch ris.TblName {
		case "rel_events":
			msg = Msg{XParId: "rel_par_id", XIdx: "rel_idx", Msg1: "Связанное событие ", Msg2: " добавлено"}
		case "duplicates":
			msg = Msg{XParId: "dup_par_id", XIdx: "dup_idx", Msg1: "Дубликат ", Msg2: " добавлен"}
		case "equals":
			msg = Msg{XParId: "eq_par_id", XIdx: "eq_idx", Msg1: "Аналог ", Msg2: " добавлен"}
	}
	PluckAndInsert(db, &ris, &msg)
	this.Data["json"] = msg.Msg1 + ris.XIdx + msg.Msg2 + " к событию " + ris.EventIdx
	this.ServeJSON()
}

//Получение информации о связанном событии
func (this *AppendToGridController) GetInfo() {
	var gi models.LastActivity
	json.Unmarshal(this.Ctx.Input.RequestBody, &gi)
	var h HeadsStruct
	var dbx *sqlx.DB
	s := GetSettings()
	dbx, _ = sqlx.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
	//Пробуем подключиться к БД
	err := dbx.Ping()
	if err == nil {
		m := map[string]interface{}{"id": gi.XParId, "user_id": this.GetSession("authorized").(int)}
		rows, err := dbx.NamedQuery(`SELECT id, heads, tbl_name, idx, name, description, start, "end", region, event_type, shift_date FROM params WHERE id= :id AND user_id= :user_id`, m)
		for rows.Next() {
		    err = rows.Scan(&h.Id, &h.Heads, &h.TblName, &h.Idx, &h.Name, &h.Description, &h.Start, &h.End, &h.Region, &h.EventType, &h.ShiftDate)
		}
		//Срез, который передается клиенту
		var rows_slice []map[string]interface{}
		heads_map := make(map[string]string)
		//Создаем reflection структуры h
		h_obj := reflect.ValueOf(&h).Elem()
		//Производим итерацию по полям структуры h, игнорируя Id int
		for i := 1; i < h_obj.NumField(); i++ {
			//Получаем значение поля Получаем тип поля
			heads_map[h_obj.Field(i).Interface().(string)] = h_obj.Type().Field(i).Name
		}
		if err == nil {
			//Считываем в rows строки, содержащие переданный тип события EventType
			rows, err := dbx.NamedQuery("SELECT * FROM "+"\""+h.TblName+"\""+" WHERE "+"\""+h.Idx+"\""+" = :xidx", gi)
			if err != nil {
				beego.Info(err)
			}
			for rows.Next() {
				//Определяем карту, в которую будут передаваться значения
			    rows_map := make(map[string]interface{})
			    err = rows.MapScan(rows_map)
			    if err == nil {
			    	//Конвертируем байты из карты в строки
					for key, value := range rows_map {
						switch value := value.(type) {
							case []uint8:
								rows_map[key] = string(value[:])
						}
						rows_map[heads_map[key]] = rows_map[key]
					}
					rows_map["HId"] = h.Id
					
					//Добавляем карты в срез
			    	rows_slice = append(rows_slice, rows_map)
			    }
			}
			this.Data["json"] = &rows_slice
		}
	} else {
		beego.Info(err)
	}
	this.ServeJSON()
}

//Сохранение последнего действия
func (this *AppendToGridController) SaveLast() {
	var la models.LastActivity
	json.Unmarshal(this.Ctx.Input.RequestBody, &la)
	session := this.GetSession("authorized")
	if session != nil {
		la.UserID = session.(int)
		s := GetSettings()
		db, _ := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
		db.AutoMigrate(&models.LastActivity{})
		var count int
		db.Model(&la).Where("user_id = ?", la.UserID).Count(&count)
		if count == 1 {
			db.Model(&la).Where("user_id = ?", la.UserID).Update(&la)
		} else {
			db.Create(&la)
		}
	}
	this.ServeJSON()
}

func PluckAndReturn(db *gorm.DB, la *models.LastActivity, msg *Msg) {
	var ids []int
	db.Table(la.TblName).Pluck("id", &ids)
	db.Exec("INSERT INTO "+la.TblName+" (id, event_par_id, event_idx, "+msg.XParId+", "+msg.XIdx+") VALUES (?,?,?,?,?);", BiggestId(ids), &la.EventParId, &la.EventIdx, &la.XParId, &la.XIdx)
}

//Отмена действий
func (this *AppendToGridController) Cancel() {
	session := this.GetSession("authorized")
	if session != nil {
		var la models.LastActivity
		s := GetSettings()
		db, _ := gorm.Open("postgres", "host="+s.Host+" user="+s.DbUser+" dbname="+s.DbName+" sslmode="+s.SslMode+" password="+s.DbPwd)
		la.UserID = session.(int)
		var count int
		db.Model(&la).Where("user_id = ?", la.UserID).Count(&count)
		if count == 1 {
			db.Model(&la).Where("user_id = ?", la.UserID).First(&la)
			switch la.Type {
				case "remove":
					var msg Msg
					switch la.TblName {
						case "rel_events":
							msg = Msg{XParId: "rel_par_id", XIdx: "rel_idx"}
						case "duplicates":
							msg = Msg{XParId: "dup_par_id", XIdx: "dup_idx"}
						case "equals":
							msg = Msg{XParId: "eq_par_id", XIdx: "eq_idx"}
						case "events":
							break	
					}
					PluckAndReturn(db, &la, &msg)
				case "insert":
					switch la.TblName {
						case "rel_events":
							db.Table("rel_events").Where("event_idx = ? AND rel_par_id = ? AND rel_idx = ?", la.EventIdx, la.XParId, la.XIdx).Delete(&la.XIdx)
						case "duplicates":
							db.Table("duplicates").Where("event_idx = ? AND dup_par_id = ? AND dup_idx = ?", la.EventIdx, la.XParId, la.XIdx).Delete(&la.XIdx)
						case "equals":
							db.Table("equals").Where("event_idx = ? AND eq_par_id = ? AND eq_idx = ?", la.EventIdx, la.XParId, la.XIdx).Delete(&la.XIdx)
						case "events":
							break	
					}
			}
			//Удаляем запись в last_activities после того, как возврат завершен
			db.Table("last_activities").Where("user_id = ?", la.UserID).Delete(&la.UserID)
			this.Data["json"] = &la		
		}
	}
	this.ServeJSON()	
}