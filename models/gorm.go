package models

import (
	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model
	Email string
	Password []byte
	Salt string
	Confirm string
	Activated bool
}

type Coord struct {
	gorm.Model
	Page string `json:"page"`
	Left int `json:"left"`
	Top int `json:"top"`
	Maximized bool `json:"maximized"`
	User User
	UserID int
}

type LastActivity struct {
	gorm.Model
	Type string `json:"type"`
	TblName string `json:"tbl_name"`
	EventParId int `json:"event_par_id"`
	EventIdx string `json:"event_idx"`
	XParId int `json:"x_par_id"`
	XIdx string `json:"x_idx"`
	User User
	UserID int `json:"user_id"`
}

type Param struct {
	gorm.Model
	ColNames string `json:"col_names"`
	DataRow string `json:"data_row"`
	Path string `json:"path"`
	TblName string `json:"tbl_name"`
	Heads string
	Idx string `json:"idx"`
	Name string `json:"name"`
	Description string `json:"description"`
	Start string `json:"start"`
	End string `json:"end"`
	Region string `json:"region"`
	EventType string `json:"event_type"`
	ShiftDate string `json:"shift_date"`
	User User
	UserID int `json:"user_id"`
}

type Settings struct {
	gorm.Model
	Host string `json:"host"`
	DbUser string `json:"db_user"`
	DbName string `json:"db_name"`
	SslMode string `json:"ssl_mode"`
	DbPwd string `json:"db_pwd"`
	DbPwdByte []byte
}