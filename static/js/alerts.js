document.addEventListener('DOMContentLoaded', function() {
	$('body').on('click','.close',function() {
		this.parentNode.style.display = 'none';
	});
	$('body').append('<div class="alert fadeout alert-success" role="alert"><button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="alert fadeout alert-danger" role="alert"><button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="esc fadeout alert-warning" role="alert"><button type="button" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
});