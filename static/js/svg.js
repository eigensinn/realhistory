document.addEventListener('DOMContentLoaded', function() {
	//Функция изменения цвета кругов и надписей по событию click
	$(".sb_svetlo, .hour_energy, .economy, .adjustablepower, .years_25, .delivery, .everyplace, .ecology, .allhome, .infinity, .mobile, .simple").click(function() {
		var cls = this.getAttribute("class").split(' ');
		["circle","text"].forEach(function(tag, i){
			var list = document.getElementsByTagName(tag);
			Array.prototype.forEach.call(list, function(el, i){
				el.classList.remove("red");
			});
		});
	    //Делаем все картинки бирюзовыми
	    var elements = document.querySelectorAll(".changed");
		Array.prototype.forEach.call(elements, function(el, i){
			var attr_r = el.getAttribute("xlink:href").split("_");
			el.setAttribute("xlink:href", attr_r[0]+".png");
			el.classList.remove("changed");
		});
	    //Меняем текущую картинку
	    if (cls[0] != "sb_svetlo") {
			var obj = document.querySelector("filter#"+cls[0]+"_a feImage");
			var attr = obj.getAttribute("xlink:href").split(".");
			obj.setAttribute("xlink:href", attr[0]+"_h.png");
			obj.classList.add("changed");
	    }
		//Добавляем цвет всему, кроме надписи на #sb_svetlo
	    $("."+cls[0]).not("text.sb_svetlo").addClass("red");
	    //Демонстрируем промо-текст
		var promo = document.querySelectorAll(".promo");
	    if($(this).is("circle,text")) {
			Array.prototype.forEach.call(promo, function(el, i){
				el.classList.add("hidden");
				if (el.classList.contains(cls[0])) {
					var el2 = document.querySelector(".promo."+cls[0]);
					el2.classList.remove("hidden");
				}
			});
	    }
	});
	//Функция для определения центра круга и компенсации сдвига по осям при его увеличении
	function defineCenter(crcl) {
		this.crcl = crcl;
		var svg = document.getElementById("svg").getBBox();
		var svg_x = 0.5*svg.x, svg_y = 0.5*svg.y;//Координаты сдвига svg-бокса, деленные пополам
		var bbox = document.getElementById(this.crcl).getBBox();
		var cx = bbox.x+(bbox.width/2), cy = bbox.y+(bbox.height/2);//Координаты центра старого круга
		var tx = -cx*0.2+svg_x, ty = -cy*0.2+svg_y;//Корректировка общего сдвига
		this.arr = [String(tx)+"px", String(ty)+"px"];
	}
	//Определяем анимацию, используя сдвиги, полученные в defineCenter
	defineCenter.prototype.keyFrameDefine = function (id, a) {
		var crcl = this.crcl;
		$.keyframe.define([{
			name: a,
			'0%':   {transform:"scale(1.0, 1.0)"},
			'50%':  {transform:"scale(1.2, 1.2) translate("+this.arr[0]+","+this.arr[1]+")"},
			'100%': {transform:"scale(1.0, 1.0)"}
		}]);
		//Добавляем анимацию по событию hover
		//var el = document.querySelector();
		$( "#"+id ).hover(
			function() {
				$("#"+crcl).playKeyframe(a+' 1s linear infinite');
			},
			function () {
				$("#"+crcl).pauseKeyframe();
			}
		);
	}
	//Определяем ассоциативный массив с именами keyFrames (они же id изображений) и id кругов
	var obj = { sb_t0: "sb_svetlo_t0", sb_t1: "sb_svetlo_t1", sb_t2: "sb_svetlo_t2", sb_a:"sb_svetlo", economy_a:"economy",
				hour_energy_a:"hour_energy", adjustablepower_a:"adjustablepower", years_25_a:"years_25", delivery_a:"delivery", everyplace_a:"everyplace",
				ecology_a:"ecology", allhome_a:"allhome", infinity_a:"infinity", mobile_a:"mobile", simple_a:"simple" };
	//Инициализируем keyFrames и filters
	$.each(obj, function(a, id) {
		var center;
		if (id == "sb_svetlo_t0" || id == "sb_svetlo_t1" || id == "sb_svetlo_t2") {
			center = new defineCenter("sb_svetlo");
		} else {
			center = new defineCenter(id);
		}
		center.keyFrameDefine(id, a);
		if (!(id=="sb_svetlo" || id=="sb_svetlo_t0" || id=="sb_svetlo_t1" || id=="sb_svetlo_t2")) {
			document.getElementById(id).setAttribute('filter', 'url(#'+a+')');
		}
	});
});