document.addEventListener('DOMContentLoaded', function() {
	//Инициализация бублика для заказа
	$("#orderChart").drawDoughnutChart([
		{ title: "ИМЯ", value: 1, color: "#f3cb58", color2: "#eeeda2"},
		{ title: "ТЕЛЕФОН",  value: 1, color: "#f6bc0e", color2: "#fdea71"},
		{ title: "EMAIL", value: 1, color: "#f3cb58", color2: "#eeeda2"},
		{ title: "КОММЕНТАРИЙ", value: 1, color: "#f6bc0e", color2: "#fdea71"},
		{ title: "КОНФИГУРАЦИИ", value: 1, color: "#f3cb58", color2: "#eeeda2"}
		], {showTip: false, summaryClass: "orderSummary", showLabel: true, summaryTitle: "Заказ", baseOffset: 0, percentageInnerCutout : 86,
		segmentStrokeWidth : 0, innerCircle: 0.86}
	);
	//Инициализация бублика с входными параметрами
	function initInputChart(obj) {
		$("#inputChart").drawDoughnutChart([
			{ title: "РАЗМЕР", value: 1, color: "#33a5a1", color2: "#5cccdb"},
			{ title: "ДОСТАВКА",  value: obj[0], color: "#2d8a96", color2: "#3dbdb9"},
			{ title: "УСТАНОВКА", value: obj[1], color: "#33a5a1", color2: "#5cccdb"},
			{ title: "СРОЧНОСТЬ", value: obj[2], color: "#2d8a96", color2: "#3dbdb9"},
			{ title: "КОЛИЧЕСТВО", value: 1, color: "#33a5a1", color2: "#5cccdb"},
			{ title: "МОЩНОСТЬ", value: 1, color: "#33a5a1", color2: "#5cccdb"},
			{ title: "ТИП", value: 1, color: "#2d8a96", color2: "#3dbdb9"}
			], {showTip: false, showLabel: true, summaryTitle: "Калькулятор стоимости", baseOffset: 0, percentageInnerCutout : 86,
			segmentStrokeWidth : 0, innerCircle: 0.86}
		);
	}
	
	//Определение свойств чарта
	function Ch(id, sum) {
		this.id = document.getElementById(id);
		var rect = this.id.getBoundingClientRect();
		this.dim = {iChartOffsetLeft: rect.left + document.body.scrollLeft, iChartOffsetTop: rect.top + document.body.scrollTop,
		iChartWidth: $(this.id).width(), iChartHeight: $(this.id).height()}
		//Установка doughnutSummary по центру
		var dS = document.querySelector(sum);
		var dS_left = String(0.5*this.dim.iChartWidth-0.5*$(dS).width()+15)+"px";
		var dS_top = String(0.5*this.dim.iChartHeight-0.5*$(dS).height())+"px";
		dS.style.top = dS_top;
		dS.style.left = dS_left;
	}
	
	//Метод перемещения лейблов
	Ch.prototype.moveLbl = function (crd, trash, pin, obj) {
		//Берем размеры и id чарта из прототипа
		var dim = this.dim;
		var id = this.id;
		//Определяем массив классов pins для данного чарта
		var pins = [pin+"1",pin+"2",pin+"3"];
		//Определяем массив соответствий: отображать pin или нет (по значению соответствующего select)
		var vals = {};
		pins.forEach(function(item, i){
			vals[item] = obj[i];
		});
		//Получаем массив div, в которых находятся selects
		var elements = document.querySelectorAll(crd);
		Array.prototype.forEach.call(elements, function(el, i){
			var cls = el.getAttribute("class").split(" ");
			//Если pin с данным классом присутствует в массиве классов и текущее значение соотвествующего select == 0
			if (pins.indexOf(cls[0]) > -1 && vals[cls[0]] == 0) {
				//Только для #inputChart
				if(cls[1] == "coord") {
					//Перемещаем div с select в trash
					el.style.position = 'relative';
					el.style.top = 'auto';
					el.style.left = 'auto';
					var trash_el = document.getElementById(trash);
					trash_el.appendChild(el);
				}
			} else {
				//Определение новых координат лейблов по координатам pins
				var pin = $("#"+cls[0])[0].getBBox();
				var pin_x = pin.x, pin_y = pin.y;
				var corr_top, corr_left;
				//Коррекция координат лейблов с учетом попадания в квадрант на svg
				var rect = id.getBoundingClientRect();
				var l = -(rect.left + document.body.scrollLeft);
				var t = -(rect.top + document.body.scrollTop);
				if (dim.iChartOffsetLeft + pin_x > dim.iChartOffsetLeft + 0.5*dim.iChartWidth) {
					if (crd == ".coord") {
						corr_left = l + 180;
					} else {
						corr_left = l + 135 + $(".wdef").width();
					}
				} else {
					if (crd == ".coord") {
						corr_left = l + 25;
					} else {
						corr_left = l - 20 + $(".wdef").width();
					}
				}
				if (dim.iChartOffsetTop + pin_y > dim.iChartOffsetTop + 0.5*dim.iChartHeight) {
					if (crd == ".coord") {
						corr_top = t + 35;
					} else {
						corr_top = t;
					}
				} else {
					if (crd == ".coord") {
						corr_top = t - 15;
					} else {
						corr_top = t - 45;
					}
				}
				//Перемещение лейблов с учетом коррекции
				el.style.position = 'absolute';
				$(el).animate({ top: dim.iChartOffsetTop + pin_y + corr_top, left: dim.iChartOffsetLeft + pin_x + corr_left });
			}
		});
	}
	
	//Перерисовка бублика по событию change на select
	$( "select" ).not("#type, #size, #calc").change(function () {
		var txt;
		//Определяем массив selects
		var obj = ["delivery", "install", "urgency"];
		//Считываем value каждого select
		obj.forEach(function(id, i){
			var el = document.getElementById(id);
			var val = el.options[el.selectedIndex].value;
			//Переписываем id selects на их значения boolean в виде цифр
			if (val == "yes") {
				obj[i] = 1;
			} else {
				obj[i] = 0;
			}
		});
		//Очищаем #inputChart
		document.getElementById('inputChart').innerHTML = '';
		//Отправляем значения selects в #inputChart (отображать сегмент или нет)
		initInputChart(obj);
		//Определяем объекты чартов, содержащие их dimensions, и перемещаем лейблы
		var iof = new Ch("inputChart", ".doughnutSummary");
		iof.moveLbl(".coord", "trashbox", "pin", obj);
		var oof = new Ch("orderChart", ".orderSummary");
		oof.moveLbl(".order_coord", "trashbox2", "order_pin", obj);
	}).change();
	
	//Show select/input
	$(".lbl").click(function(){
		$(this).siblings().slideToggle("slow");
		var el = this.parentNode.querySelector('select, input, textarea');
		var cls = el.getAttribute("class").split("_")[0];
		$(this).toggleClass( cls+"_c", "gray" );
	});
	//Открывает все кнопки управления
	$('#btn_ctrl').click(function(){
		$("#size, #delivery, #install, #urgency, #quantity, #type, #power, #nm, #phone, #email, #comment").slideToggle("slow");
		var elements = document.querySelectorAll(".lbl");
		Array.prototype.forEach.call(elements, function(outer, i){
			var el = outer.parentNode.querySelector('select, input, textarea');
			var cls = el.getAttribute("class").split("_")[0];
			$(cls).toggleClass( cls+"_c", "gray" );
		});
	});
	//Подсказки
	var arr = {'.pin4 .lbl':'Начните набирать количество. При смене количества мощность будет вычисляться автоматически, используя текущие тип и размер',
			   '.pin5 .lbl':'Наберите необходимую мощность. Затем нажмите enter или просто кликните вне поля. Будет установлена наиболее близкая возможная мощность при кратности, задаваемой значением размера',
			   '#btn_ctrl':'Открыть все поля калькулятора и формы заказа для ввода данных',
			   '#btn_calc':'Рассчитать заказ',
			   '#btn_save':'Сохранить данные заказа в виде конфигурации, которая также будет доступна в кабинете',
			   '#btn_order':'Сделать заказ (можно отменить в кабинете)'}
	$.each(arr, function(obj, txt) {
		$(obj).qtip({
			content: {
				text: txt
			},
			style: {
				classes: 'qtip-gray'
			}
		});
	});
	//Маска номера телефона
	$('#phone').mask('+7 000 000-00-00');
});