document.addEventListener('DOMContentLoaded', function() {
	Sideshow.registerWizard({
		name: "upload_tables",
		title: "1 Загрузка таблиц",
		description: "Здесь Вы научитесь импортировать таблицы Excel (.xlsx)",
		estimatedTime: "10 мин.",
		affects: [
			{ hash: "#messages" }, //This tutorial would be eligible for URLs like this "http://www.foo.com/bar#messages"
			{ route: "/events/xlsx", caseSensitive: true },  //This tutorial would be eligible for URLs like this "http://www.foo.com/adm/orders"
			function(){
				return $(".grid").length > 0;
			}   
		],
		relatedWizards: [ "edit_tables" ]
	}).storyLine({
		showStepPosition: true,
		steps: [
			{
				title: "Создайте файл",
				text: function(){/*
Первое, что Вам необходимо сделать – создать файл Microsoft Excel (.xlsx), содержащий данные событий.
				
Для успешной загрузки на сервер и обеспечения возможности дальнейшего редактирования к структуре файла предъявляется несколько требований:

* Таблица должна находиться на первом листе (по ходу развития сайта будет внедрена возможность читать таблицы и с других листов)
* Одна из строк должна содержать имена столбцов, сами данные должны находиться ниже этой строки. Выше этой строки может находиться все, что угодно, например название таблицы
* В первом столбце каждой строки должен быть номер (могут идти не по порядку и не совпадать с номерами строк). Эти номера будут преобразованы в первичные ключи в базе данных

Не так уж и много требований для совместимости ваших файлов и нашей базы данных, не правда ли?
				*/},
				format: "markdown"
			},
			{
				title: "Укажите параметры",
				text: "Введите номер строки, содержащей имена столбцов, и номер строки, с которой начинаются данные. В учебном примере это строки 2 и 3",
				subject: "#fm_param",
				targets: "#fm_param > input",
				autoContinue: false,
				completingConditions: [
					function(){
						var selectors = document.querySelectorAll("#fm_param > input");
						var conditions = [];
						Array.prototype.forEach.call(selectors, function(el, i){
							var val = el.value;
							//Если строка не пустая и значение - число
							if(val != "" && Number(val) === parseInt(val, 10)) {
								conditions[i] = parseInt(val, 10);
							}
						});
						//Если обе сроки правильно заполнены и значение во второй строке больше, чем в первой
						return conditions.length > 1 && conditions[1] > conditions[0];
					}
				]
			},
			{
				title: "Загрузите файл",
				text: "Нажмите на кнопку `Загрузить файл...` – начнется процесс загрузки файла на сервер и его конвертации. Процесс может идти долго. Таблице будет присвоено имя, состоящее из уникального идентификатора и исходного имени. Это сделано на случай, если Вам вдруг захочется загрузить много таблиц с одинаковыми именами",
				subject: "#upload_wrap",
				targets: ".fileinput-button",
				autoContinue: false,
				completingConditions: [
					function(){
						return upload_complete > 0;
					}
				]
			},
			{
				title: "Выберите Вашу таблицу",
				text: "Для отображения таблицы следует выбрать ее из списка доступных таблиц",
				subject: "#table_choice",
				targets: "#tbl_names",
				autoContinue: false,
				completingConditions: [
					function(){
						var table = document.getElementById("tbl");
						return table.rows.length > 1;
					}
				]
			},
			{
				title: "Заполните таблицу соответствий",
				text: "Таблица соответствий создана с той целью, чтобы программа на сервере понимала, каким образом воспринимать каждый столбец Вашей таблицы: программа знает только ограниченное количество типов полей. Просто перетащите пару заголовков таблицы в поля `Id` и `Тип события` таблицы соответствий",
				subject: "#tables",
				targets: "#heads th, #event_type, #idx",
				autoContinue: false,
				completingConditions: [
					function(){
						var event_type = document.getElementById("event_type");
						var idx = document.getElementById("idx");
						return event_type.childNodes.length > 0 && event_type.childNodes[0].innerHTML != "None" && idx.childNodes.length > 0 && idx.childNodes[0].innerHTML != "None";
					}
				]
			},
			{
				title: "Поздравляем!",
				text: "После загрузки таблицы на сервер и установки полей `Id` и `Тип события`, по которым на данный момент производится большая часть выборок, с сайтом можно работать дальше"
			}
		]
	});
	Sideshow.registerWizard({
		name: "edit_tables",
		title: "2 Редактирование таблиц",
		description: "Здесь Вы научитесь добавлять, удалять и править строки таблиц",
		estimatedTime: "5 минут",
		affects: [
			{ hash: "#messages" },
			{ route: "/events/xlsx", caseSensitive: true },
			function(){
				return $(".grid").length > 0;
			}   
		],
        relatedWizards: [ "upload_tables" ]
	}).storyLine({
		showStepPosition: true,
		steps: [
			{
				title: "Выберите строку таблицы",
				text: "Выберите строку таблицы, кликнув на нее один раз.",
                subject: "#tbl",
                targets: "#tbl tbody tr:first-child",
                autoContinue: false,
				completingConditions: [
					function(){
						var row = document.querySelector("#tbl tbody tr:first-child");
						return row.classList.contains("selected");
					}
				]
			},
			{
				title: "Удалите строку",
				text: "Удалите строку, нажав на кнопку `Удалить строку`",
                subject: ".dialog-box",
                targets: "#btn_rem_row",
                autoContinue: false,
                listeners: {
					afterStep:
					function(){
						var el = document.getElementById('btn_rem_row');
						el.classList.remove("clicked");
						return false;
					}
				},
				completingConditions: [
					function(){
						var el = document.getElementById('btn_rem_row');
						$("#btn_rem_row").click(function() {
							this.classList.add("clicked");
						});
						return el.classList.contains("clicked");
					}
				]
			},
            {
				title: "Добавьте строку",
				text: "Добавьте строку, кликнув на кнопку `Добавить строку`",
                subject: ".dialog-box",
                targets: "#btn_add_row",
                autoContinue: false,
                listeners: {
					afterStep:
					function(){
						var el = document.getElementById('btn_add_row');
						el.classList.remove("clicked");
						return false;
					}
				},
				completingConditions: [
					function(){
						var el = document.getElementById('btn_add_row');
						$("#btn_add_row").click(function() {
							this.classList.add("clicked");
						});
						return el.classList.contains("clicked");
					}
				]
			},
            {
				title: "Введите Foreign key",
				text: "Введите Foreign key, кликнув два раза",
                subject: "#tbl",
                targets: "#tbl tbody tr:last-child td:first-child",
                autoContinue: false,
				completingConditions: [
					function(){
                        var td = document.querySelector("#tbl tbody tr:last-child td:first-child");
                        var condition = false;
                        var html = td.innerHTML;
                        //Если строка не пустая и значение - число
                        if(html != "" && html != "–" && Number(html) === parseInt(html, 10) && td.classList.contains("updated")) {
                            condition = true;
                        }
						return condition == true;
					}
				]
			},
            {
				title: "Введите имя поля",
				text: "Введите имя поля, кликнув два раза по второму столбцу",
                subject: "#tbl",
                targets: "#tbl tbody tr:last-child td:nth-child(2)",
                autoContinue: false,
				completingConditions: [
					function(){
                        var td = document.querySelector("#tbl tbody tr:last-child td:nth-child(2)");
                        var condition = false;
                        var html = td.innerHTML;
                        //Если строка не пустая
                        if(html != "" && html.length > 1 && td.classList.contains("updated")) {
                            condition = true;
                        }
						return condition == true;
					}
				]
			},
            {
				title: "Сохраните строку",
				text: "Сохраните строку, кликнув на кнопку `Сохранить`",
                subject: ".dialog-box",
                targets: "#btn_save",
                autoContinue: false,
                listeners: {
					afterStep:
					function(){
						var el = document.getElementById('btn_save');
						el.classList.remove("clicked");
						return false;
					}
				},
				completingConditions: [
					function(){
						var el = document.getElementById('btn_save');
						$("#btn_save").click(function() {
							this.classList.add("clicked");
						});
						return el.classList.contains("clicked");
					}
				]
			},
            {
				title: "Поздравляем!",
				text: "Вы научились редактировать таблицы!"
			}
		]
	});
	Sideshow.registerWizard({
		name: "event_operations",
		title: "1 Операции с событиями",
		description: "Здесь Вы научитесь просматривать события и удалять их",
		estimatedTime: "10 мин.",
		affects: [
			{ hash: "#messages" },
			{ route: "/events/grid", caseSensitive: true },
			function(){
				return $(".grid").length > 0;
			}   
		],
		relatedWizards: [ "rel_event_operations", "equals_operations" ]
	}).storyLine({
		showStepPosition: true,
		steps: [
			{
				title: "Выберите таблицу и тип события",
				text: "Выберите таблицу, события из которой Вы хотели бы просмотреть. На данный момент поддерживается фильтр по типу события. Тип события `*` означает выбор всех типов одновременно. После выбора опций нажмите на `Показать`",
				subject: "#table-change",
				targets: "#tblname, #eventtype, #btn-show",
				autoContinue: false,
				listeners: {
					afterStep:
					function(){
						var el = document.getElementById('btn-show');
						el.classList.remove("clicked");
						return false;
					}
				},
				completingConditions: [
					function(){
						var el = document.getElementById('btn-show');
						//Только jQuery. После vanillaJS не закружаются виджеты
						$("#btn-show").click(function() {
							this.classList.add("clicked");
						});
						return el.classList.contains("clicked");
					}
				]
			},
			{
				title: "Откройте виджет события",
				text: "Виджет события открывается нажатием на `стрелку вниз`. Откройте виджет.",
				subject: ".panel:first-child",
				targets: ".panel:first-child > .expand > .glyphicon-chevron-down",
				autoContinue: false,
				completingConditions: [
					function(){
						var up = document.querySelector(".panel:first-child > .expand > span");
						return up.classList.contains("glyphicon-chevron-up");
					}
				]
			},
			{
				title: "Откройте информацию о событии",
				text: "Информация о событии также открывается нажатием на `стрелку вниз`. Откройте информацию.",
				subject: ".panel:first-child",
				targets: ".panel:first-child > .panel-body > .expand > .glyphicon-chevron-down",
				autoContinue: false,
				listeners: {
					afterStep: function(){
						var up = document.querySelector(".panel:first-child > .expand").previousSibling;
						up.classList.remove("pbody-opened");
						up.classList.add("pbody-closed");
						var pbody = document.querySelector(".panel:first-child > .panel-body > .expand").previousSibling;
						pbody.classList.remove("pbody-opened");
						pbody.classList.add("pbody-closed");
						var exp = document.querySelector(".panel:first-child > .panel-body > .expand > span");
						exp.classList.remove("glyphicon-chevron-down");
						exp.classList.add("glyphicon-chevron-up");
						return false;
					}
				},
				completingConditions: [
					function(){
						var up = document.querySelector(".panel:first-child > .panel-body > .expand > span");
						return up.classList.contains("glyphicon-chevron-up");
					}
				]
			},
			{
				title: "Удалите событие",
				text: function(){/*
Удалить событие можно двумя способами:

1. С помощью кнопки "Удалить"
2. Выбросив заголовок события из виджета
				*/},
				format: "markdown",
				subject: ".panel:first-child",
				targets: ".panel:first-child .events button.rem_evnt_btn",
				autoContinue: false,
				listeners: {
					afterStep: function(){
						$('#rem_evnt_mod').modal('hide');
						var rem = document.querySelector(".panel:first-child .events button.rem_evnt_btn");
						rem.classList.remove("clicked");
						return false;
					}
				},
				completingConditions: [
					function(){
						var rem = document.querySelector(".panel:first-child .events button.rem_evnt_btn");
						rem.onclick = function() {
							rem.classList.add("clicked");
						};
						return rem.classList.contains("clicked");
					}
				]
			},
			{
				title: "Переключитесь в режим `Правка`",
				text: "Для того, чтобы заголовки стали сдвигаемыми, необходимо перейти в режим `Правка`",
				subject: "#controls",
				targets: "#controls p:nth-child(3) .toggle",
				autoContinue: false,
				completingConditions: [
					function(){
						return document.getElementById("btn-edit").parentNode.classList.contains("btn-success");
					}
				]
			},
			{
				title: "Удалите событие",
				text: "Удалите событие, выбросив заголовок из виджета",
				subject: ".panel:first-child",
				targets: ".panel:first-child .events .handle",
				autoContinue: false,
				completingConditions: [
					function(){
						return document.querySelector(".panel:first-child .events").childNodes.length < 2;
					}
				]
			}
		]
	});
	Sideshow.registerWizard({
		name: "rel_event_operations",
		title: "2 Добавление связей",
		description: "Здесь Вы научитесь добавлять связанные события и исторические дубликаты",
		estimatedTime: "10 мин.",
		affects: [
			{ hash: "#messages" },
			{ route: "/events/grid", caseSensitive: true },
			function(){
				return $(".grid").length > 0;
			}   
		],
        relatedWizards: [ "event_operations, equals_operations" ]
	}).storyLine({
		showStepPosition: true,
		steps: [
			{
				title: "Загрузите события",
				text: "Загрузите события, нажав на кнопку `Показать`",
				subject: "#table-change",
				targets: "#btn-show",
				autoContinue: false,
				listeners: {
					afterStep:
					function(){
						var el = document.getElementById('btn-show');
						el.classList.remove("clicked");
						return false;
					}
				},
				completingConditions: [
					function(){
						var el = document.getElementById('btn-show');
						$("#btn-show").click(function() {
							this.classList.add("clicked");
						});
						return el.classList.contains("clicked");
					}
				]
			},
            {
				title: "Откройте виджет события",
				text: "Виджет события открывается нажатием на `стрелку вниз`. Откройте виджет.",
				subject: ".panel:first-child",
				targets: ".panel:first-child > .expand > .glyphicon-chevron-down",
				autoContinue: false,
				completingConditions: [
					function(){
						var up = document.querySelector(".panel:first-child > .expand > span");
						return up.classList.contains("glyphicon-chevron-up");
					}
				]
			},
            {
				title: "Переключитесь в режим `Правка`",
				text: "Для того, чтобы заголовки стали сдвигаемыми, необходимо перейти в режим `Правка`",
				subject: "#controls",
				targets: "#controls p:nth-child(3) .toggle",
				autoContinue: false,
				completingConditions: [
					function(){
						return document.getElementById("btn-edit").parentNode.classList.contains("btn-success");
					}
				]
			},
            {
				title: "Создайте связь",
				text: "Возьмите событие за заголовок и перетащите его в `Связанные события`",
				subject: "#grid",
				targets: ".panel:nth-child(2) .events span .handle, .panel:first-child .panel-body .rel_events",
				autoContinue: false,
				completingConditions: [
					function(){
						return document.querySelector(".panel:first-child .panel-body .rel_events").childNodes.length > 0;
					}
				]
			},
            {
				title: "Добавьте дубликат",
				text: "Возьмите созданное связанное событие за белый треугольник и перетащите его в `Исторические дубликаты`",
				subject: "#grid",
				targets: ".panel:first-child .panel-body .rel_events > span:first-child .handle, .panel:first-child .panel-body .duplicates",
				autoContinue: false,
				completingConditions: [
					function(){
						return document.querySelector(".panel:first-child .panel-body .duplicates").childNodes.length > 0;
					}
				]
			},
            {
				title: "Поздравляем!",
				text: "Вы научились добавлять связанные события и исторические дубликаты."
			}
		]
	});
    Sideshow.registerWizard({
		name: "equals_operations",
		title: "3 Добавление аналогов",
		description: "Здесь Вы научитесь добавлять аналоги событий из других таблиц",
		estimatedTime: "10 мин.",
		affects: [
			{ hash: "#messages" },
			{ route: "/events/grid", caseSensitive: true },
			function(){
				return $(".grid").length > 0;
			}   
		],
        relatedWizards: [ "event_operations, rel_event_operations" ]
	}).storyLine({
		showStepPosition: true,
		steps: [
            {
				title: "Переключитесь в режим накапливания данных",
				text: "Для того, чтобы загружать события из разных таблиц, необходимо перейти в режим `Накапл. данн.`",
				subject: "#controls",
				targets: "#controls p:nth-child(2) .toggle",
				autoContinue: false,
				completingConditions: [
					function(){
						return document.getElementById("btn-collect").parentNode.classList.contains("btn-success");
					}
				]
			},
			{
				title: "Загрузите события из первой таблицы",
				text: "Загрузите события из первой таблицы, нажав на кнопку `Показать`",
				subject: "#table-change",
				targets: "#btn-show",
				autoContinue: false,
				listeners: {
					afterStep:
					function(){
						var el = document.getElementById('btn-show');
						el.classList.remove("clicked");
						return false;
					}
				},
				completingConditions: [
					function(){
						var el = document.getElementById('btn-show');
						$("#btn-show").click(function() {
							this.classList.add("clicked");
						});
						return el.classList.contains("clicked");
					}
				]
			},
            {
				title: "Выберите вторую таблицу",
				text: "Выберите вторую таблицу. После выбора опций нажмите на `Показать`. События из второй таблицы будут добавлены к событиям из первой",
				subject: "#table-change",
				targets: "#tblname, #btn-show",
				autoContinue: false,
				listeners: {
					afterStep:
					function(){
						var ch = document.getElementById('btn-show');
						ch.classList.remove("changed");
                        var sh = document.getElementById('btn-show');
                        sh.classList.remove("clicked");
						return false;
					}
				},
				completingConditions: [
					function(){
						var sh = document.getElementById('btn-show');
                        var ch = document.getElementById('tblname');
						$("#tblname").change(function() {
							this.classList.add("changed");
						});
                        $("#btn-show").click(function() {
							this.classList.add("clicked");
						});
						return sh.classList.contains("clicked") && ch.classList.contains("changed");
					}
				]
			},
            {
				title: "Переключитесь в режим `Правка`",
				text: "Для того, чтобы заголовки стали сдвигаемыми, необходимо перейти в режим `Правка`",
				subject: "#controls",
				targets: "#controls p:nth-child(3) .toggle",
				autoContinue: false,
				completingConditions: [
					function(){
						return document.getElementById("btn-edit").parentNode.classList.contains("btn-success");
					}
				]
			},
            {
				title: "Откройте виджет события",
				text: "Виджет события открывается нажатием на `стрелку вниз`. Откройте виджет.",
				subject: ".panel:first-child",
				targets: ".panel:first-child > .expand > .glyphicon-chevron-down",
				autoContinue: false,
				completingConditions: [
					function(){
						var up = document.querySelector(".panel:first-child > .expand > span");
						return up.classList.contains("glyphicon-chevron-up");
					}
				]
			},
            {
				title: "Добавьте аналог",
				text: "Возьмите любое событие из второй таблицы и перетащите его заголовок за белый треугольник в `Аналоги в таблицах`",
				subject: "#grid",
				targets: ".panel:first-child .panel-body .equals",
				autoContinue: false,
				completingConditions: [
					function(){
						return document.querySelector(".panel:first-child .panel-body .equals").childNodes.length > 0;
					}
				]
			},
            {
				title: "Поздравляем!",
				text: "Вы научились добавлять аналоги событий из других таблиц."
			}
		]
	});
	Sideshow.registerWizard({
		name: "show_graph",
		title: "Просмотр графиков",
		description: "Здесь Вы научитесь просматривать события в виде графиков",
		estimatedTime: "10 мин.",
		affects: [
			{ hash: "#messages" },
			{ route: "/events/flot", caseSensitive: true },
			function(){
				return $(".grid").length > 0;
			}   
		],
        relatedWizards: [ "edit_graph" ]
	}).storyLine({
		showStepPosition: true,
		steps: [
            {
                title: "Выберите таблицу и тип события",
				text: "Выберите таблицу и тип события",
				subject: "#table-change",
				targets: "#tblname, #eventtype"
            },
            {
                title: "Просмотрите график",
				text: "Нажмите на `График`, чтобы просмотреть выбранные события в виде графика",
				subject: ".dialog-box",
				targets: "#btn-graph",
				listeners: {
					afterStep:
					function(){
						var el = document.getElementById('btn-graph');
						el.classList.remove("clicked");
						return false;
					}
				},
				completingConditions: [
					function(){
						var el = document.getElementById('btn-graph');
						$("#btn-graph").click(function() {
							this.classList.add("clicked");
						});
						return el.classList.contains("clicked");
					}
				]
            },
            {
                title: "Отображение событий",
				text: "События отобразятся в виде горизонтальных столбиков.",
				subject: "#touchflot"
            },
            {
				title: "Включите отображение дубликатов",
				text: "Для того, чтобы включить отображение дубликатов, нажмите на `Дубликаты`",
				subject: ".dialog-box",
				targets: ".dialog-box .dialog-content .toggle",
				completingConditions: [
					function(){
						return document.getElementById("btn-dup").parentNode.classList.contains("btn-success");
					}
				]
			},
            {
				title: "Отображение дубликатов",
				text: "Присутствующие дубликаты будут связаны красными линиями",
				subject: "#touchflot"
				
			},
            {
                title: "Выберите временной интервал",
				text: "Для просмотра событий, произошедших в определенные годы, выберите даты начала и конца временного интервала, нажав на иконку `календарик` или просто напечатайте их в input-полях",
				subject: "#date-change",
				targets: "#start, #end, .input-group-addon",
                autoContinue: false,
				completingConditions: [
					function(){
						var selectors = document.querySelectorAll("input.form-control");
						var conditions = [];
						Array.prototype.forEach.call(selectors, function(el, i){
							var val = el.value;
							//Если строка не пустая и значение - число
							if(val != "" && Number(val) === parseInt(val, 10)) {
								conditions[i] = parseInt(val, 10);
							}
						});
						//Если обе сроки правильно заполнены и значение во второй строке больше, чем в первой
						return conditions.length > 1 && conditions[1] > conditions[0];
					}
				]
            },
            {
                title: "Просмотрите график",
				text: "Снова нажмите на `График`",
				subject: ".dialog-box",
				targets: "#btn-graph",
				listeners: {
					afterStep:
					function(){
						var el = document.getElementById('btn-graph');
						el.classList.remove("clicked");
						return false;
					}
				},
				completingConditions: [
					function(){
						var el = document.getElementById('btn-graph');
						$("#btn-graph").click(function() {
							this.classList.add("clicked");
						});
						return el.classList.contains("clicked");
					}
				]
            },
            {
				title: "Отображение событий из временного интервала",
				text: "Отображены события только из выбранного временного интервала",
				subject: "#touchflot"
			},
            {
				title: "Поздравляем!",
				text: "Вы научились просматривать события в графическом виде."
			}
        ]
    });
    Sideshow.registerWizard({
		name: "edit_graph",
		title: "Редактирование графиков",
		description: "Здесь Вы научитесь изменять даты событий",
		estimatedTime: "3 мин.",
		affects: [
			{ hash: "#messages" },
			{ route: "/events/flot", caseSensitive: true },
			function(){
				return $(".grid").length > 0;
			}   
		],
        relatedWizards: [ "show_graph" ]
	}).storyLine({
		showStepPosition: true,
		steps: [
            {
                title: "Отобразите график",
				text: "Нажмите на `График`",
				subject: ".dialog-box",
				targets: "#btn-graph",
                autoContinue: false,
				listeners: {
					afterStep:
					function(){
						var el = document.getElementById('btn-graph');
						el.classList.remove("clicked");
						return false;
					}
				},
				completingConditions: [
					function(){
						var el = document.getElementById('btn-graph');
						$("#btn-graph").click(function() {
							this.classList.add("clicked");
						});
						return el.classList.contains("clicked");
					}
				]
            },
            {
                title: "Сдвиньте дату",
				text: "Сдвиньте дату начала или окончания любого события, потянув за соответствующий его край",
				subject: "#touchflot",
                autoContinue: false,
				listeners: {
                    beforeStep:
                    function(){
                        var el = document.getElementById('touchflot');
                        el.classList.add("not_changed");
					    return false;
					},
                    afterStep:
					function(){
						var tf = document.getElementById('touchflot');
                        if (tf.classList.contains('not_changed')){
                            tf.classList.remove("not_changed");
                        }
						return false;
					}
				},
				completingConditions: [
					function(){
						var el = document.getElementById('touchflot');
						return !el.classList.contains("not_changed");
					}
				]
            },
            {
				title: "Поздравляем!",
				text: "Вы научились редактировать графики."
			}
        ]
    });
	$("#run").click(function(){
		Sideshow.start({ listAll: true });
	});
	Sideshow.config.language = "en";
	Sideshow.init();
});