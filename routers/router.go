package routers

import (
	"realhistory/controllers"
	"github.com/astaxie/beego"
)

func init() {
    beego.Router("/", &controllers.MainController{})
    beego.Router("/xlsxtosql", &controllers.XLSXtoSQLController{}, "post:XLSXtoSQL")
    beego.Router("/events/xlsx", &controllers.XLSXController{})
    beego.Router("/events/grid", &controllers.GridController{})
    beego.Router("/appendtogrid", &controllers.AppendToGridController{}, "post:AppendToGrid")
    beego.Router("/eventremove", &controllers.AppendToGridController{}, "post:EventRemove")
    beego.Router("/relinsert", &controllers.AppendToGridController{}, "post:RelInsert")
    beego.Router("/getinfo", &controllers.AppendToGridController{}, "post:GetInfo")
    beego.Router("/savelast", &controllers.AppendToGridController{}, "post:SaveLast")
    beego.Router("/cancel", &controllers.AppendToGridController{}, "post:Cancel")
    beego.Router("/events/flot", &controllers.FlotController{})
    beego.Router("/appendtoflot", &controllers.AppendToFlotController{}, "post:AppendToFlot")
    beego.Router("/eventchange", &controllers.AppendToFlotController{}, "post:EventChange")
    beego.Router("/createuser", &controllers.AuthController{}, "post:CreateUser")
    beego.Router("/auth", &controllers.AuthController{}, "post:Auth")
    beego.Router("/confirm/:word([A-Za-z]+)", &controllers.AuthController{}, "get:Confirm")
    beego.Router("/logout", &controllers.AuthController{}, "get:Logout")
    beego.Router("/savecoord", &controllers.CoordController{}, "post:SaveCoord")
    beego.Router("/getcoord", &controllers.CoordController{}, "post:GetCoord")
    beego.Router("/fill", &controllers.FillController{}, "post:Fill")
    beego.Router("/heads", &controllers.FillController{}, "post:Heads")
    beego.Router("/removerowact", &controllers.ActionController{}, "post:RemoveRowAct")
    beego.Router("/saveact", &controllers.ActionController{}, "post:SaveAct")
    beego.Router("/sendcol", &controllers.ActionController{}, "post:SendCol")
    beego.Router("/setsettings", &controllers.SettingsController{}, "post:SetSettings")
    beego.Router("/info", &controllers.InfoController{})
    beego.Router("/tech", &controllers.TechController{})
    //beego.Router("/calculator", &controllers.PanelController{}, "get:GetPanel;post:PostPanel")
}
