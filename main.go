package main

import (
	_ "realhistory/routers"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
	//_ "github.com/astaxie/beego/session/postgres"
)

func init() {
    //beego.BConfig.WebConfig.Session.SessionProvider = "postgresql"
	//beego.BConfig.WebConfig.Session.SessionProviderConfig = "gorm://gorm:G%rmPwd777@localhost/tmp?sslmode=verify-full"
}

var loginCheck = func(ctx *context.Context) {
    _, ok := ctx.Input.Session("authorized").(int)
    if !ok {
        ctx.Redirect(302, "/")
    }
}

func main() {
	beego.InsertFilter("/events/*", beego.BeforeRouter, loginCheck)
	beego.Run()
}
