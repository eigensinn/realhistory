# Realhistory (локальная версия)

Это offline-версия сайта, специально предназначенного для хранения и структурирования информации по историческим исследованиям.

Сайт предоставляет возможности:

* импорта неограниченного количества таблиц исторических событий из формата MS Excel(.xlsx) в БД PostgreSQL;
* установки логических связей событий между собой в рамках одной таблицы;
* установки кросстабличных логических связей;
* фиксации дубликатов исторических событий;
* просмотра и редактирования событий в виде таблиц, виджетов и графиков;
* обучения с помощью встроенного интерактивного программного компонента.
                
## Краткое руководство по развертыванию

1 Скомпилированные версии сайта, предназначенные для работы под Microsoft Windows, можно получить по [ссылке](https://drive.google.com/open?id=0B9Bap8htsA28YXRZem8wVEJJeTg).

2 Для работы с локальной версии сайта требуется предварительная установка БД PostgreSQL 9.5, создание пользователя БД для ORM и создание пустой БД с кодировкой UTF-8 для хранения данных.

3 После подготовки БД требуется запустить файл realhistory.exe, зайти по адресу [127.0.0.1:8080](http://127.0.0.1:8080), кликнуть по значку "Настройки" и добавить реквизиты своей БД на сайт, например:

* Host: 127.0.0.1
* DbName: gorm
* DbPwd: G%rmPwd777
* DbUser: gorm
* SslMode: disable

4 Для работы с сайтом требуется регистрация с использованием реального email, на который приходит ссылка для подтверждения регистрации. __Данные по регистрациям не собираются__. Все данные для авторизации сохраняются исключительно в Вашей локальной БД.

5 Для запуска обучения на страницах [Таблица](/views/xlsx/xlsx.tpl), [Сетка](/views/grid/grid.tpl) и [Графики](/views/flot/flot.tpl) нажмите F2.

6 Если Вы желаете собрать сайт самостоятельно и использовать свой smtp-сервер для процедуры регистрации, Вы можете указать его реквизиты в файле [/controllers/auth.go](/controllers/auth.go).

## Лицензия

Исходный код сайта предоставляется по лицензии [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html).

## Благодарности

В исходном коде сайта использованы следующие сторонние программные компоненты:

* #####Сервер:       
    * [Beego Framework](https://github.com/astaxie/beego)
    * [gorm – ORM Library](https://github.com/jinzhu/gorm)
    * [sqlx – General purpose extensions to golang's database/sql/ORM Library](https://github.com/jmoiron/sqlx)
    * [xlsx – golang library for reading and writing XLSX files](https://github.com/tealeg/xlsx)
    * [chardet – Charset detector library for golang derived from ICU](https://github.com/saintfish/chardet)
    * [golang-set – A simple set type for the Go language](https://github.com/deckarep/golang-set)
            
* #####Клиент:
    * [jQuery 2](https://jquery.com)
    * [Bootstrap 3](http://getbootstrap.com)
    * [bootstrap-datetimepicker – Date/time picker widget based on twitter bootstrap](https://github.com/Eonasdan/bootstrap-datetimepicker)
    * [dragula – Drag'n Drop Library](https://github.com/bevacqua/dragula)
    * [DataTables – Table plugin for jQuery](https://datatables.net)
    * [paginationjs – A jQuery plugin to provide simple yet fully customisable pagination](https://github.com/superRaytin/paginationjs)
    * [ftdomdelegate – DOM event delegator](https://github.com/ftlabs/ftdomdelegate)
    * [Sideshow – javascript library which aims to reduce your user's learning curve by providing a way to create step-by-step interactive tours](https://github.com/fortesinformatica/Sideshow)
    * [flot – Attractive JavaScript charts for jQuery](https://github.com/flot/flot)
    * [JUMFlot library and plugins](http://jumflot.jumware.com/Experimental/Download.html)
    * [FileSaver – An HTML5 saveAs() FileSaver implementation](https://github.com/eligrey/FileSaver.js)
    * [reqwest – browser asynchronous http requests](https://github.com/ded/reqwest)
    * [philter – CSS filters with HTML attributes](http://specro.github.io/Philter/)
    * [dialogbox](https://plus.google.com/108949996304093815163/about)