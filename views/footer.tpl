<footer>
	<div class="row">
		<div class="col-lg-2 col-md-2">
			<img id="logo_footer" src="/static/img/logo.png">
			<p class="text-center year">2017</p>
		</div>
		<div class="col-lg-8 col-md-8 row emer_hover_cover">
            <a class="col-lg-6 col-md-6 map_cell" href="/info">Помощь / Информация</a>
			<a id="lnk_tech" class="col-lg-6 col-md-6 map_cell" href="/tech">Технологии / Лицензия</a>
			<a class="col-lg-6 col-md-6 map_cell" href="#">Карта сайта</a>
			<a class="col-lg-6 col-md-6 map_cell" href="#">Контакты</a>
		</div>
		<div class="col-lg-2 col-md-2">
			<p class="emer_c">Нужна помощь? Пишите!</p>
			<p><a class="emer_c contact_mail" href="mailto:necrosfodel@mail.ru">necrosfodel@mail.ru</a></p>
			<button type="button" id="telegram" class="btn btn-default btn-lg"><span class="socicon-telegram"></span></button>
			<button type="button" id="facebook" class="btn btn-default btn-lg"><span class="socicon-facebook"></span></button>
			<button type="button" id="twitter" class="btn btn-default btn-lg"><span class="socicon-twitter"></span></button>
		</div>
	</div>
</footer>