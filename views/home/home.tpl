<div class="row">
	<div class="col-lg-12 col-md-12 row">
		<div class="col-lg-12 col-md-12 row strip">
			<div data-philter-contrast="70 100">
				<img class="col-lg-12 col-md-12" src="/static/img/stone.jpg">
			</div>
		</div>
		<div class="row col-lg-12 col-md-12 strip">
			<div class="col-lg-6 col-md-6">
				<p>Это offline-версия сайта, специально предназначенного для хранения и структурирования информации по историческим исследованиям.</p>
			</div>
			<div data-philter-contrast="70 100">
				<img class="image_t col-lg-3 col-md-3" src="/static/img/live_chess.jpg">
			</div>
			<div data-philter-contrast="70 100">
				<img class="image_r col-lg-3 col-md-3" src="/static/img/borodino.jpg">
			</div>
		</div>
		<div class="row col-lg-12 col-md-12">
			<div data-philter-contrast="70 100">
				<img class="image_l col-lg-3 col-md-3" src="/static/img/beauty.jpg">
			</div>
			<div data-philter-contrast="70 100">
				<img class="image_b col-lg-3 col-md-3" src="/static/img/eiffel_mosque.jpg">
			</div>
			<div class="col-lg-6 col-md-6">
				<p>Сайт предоставляет возможности:</p>
				<ul>
                    <li>импорта неограниченного количества таблиц исторических событий из формата MS Excel(.xlsx) в БД PostgreSQL;</li>
					<li>установки логических связей событий между собой в рамках одной таблицы;</li>
                    <li>установки кросстабличных логических связей;</li>
					<li>фиксации дубликатов исторических событий;</li>
					<li>просмотра и редактирования событий в виде таблиц, виджетов и графиков;</li>
                    <li>обучения с помощью встроенного интерактивного программного компонента.</li>
				</ul>
			</div>
		</div>
	</div>
</div>