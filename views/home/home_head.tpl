<title>Домашняя страница</title>
<script src="/static/js/philter.min.js"></script>
<script>
	document.addEventListener('DOMContentLoaded', function() {
		var off = "900px";
		var image_r = document.querySelector(".image_r");
		image_r.style.left = off;
		$(image_r).animate({"left": "-="+off}, 1000);
		var image_l = document.querySelector(".image_l");
		image_l.style.left = "-"+off;
		$(image_l).animate({"left": "+="+off}, 1000);
		var image_t = document.querySelector(".image_t");
		image_t.style.top = "-"+off;
		$(image_t).animate({"top": "+="+off}, 1000);
		var image_b = document.querySelector(".image_b");
		image_b.style.top = off;
		$(image_b).animate({"top": "-="+off}, 1000);
		new Philter({transitionTime: 0.4});
	});
</script>