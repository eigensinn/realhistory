<div class="row">
	<div class="col-lg-offset-2 col-md-offset-2 col-lg-8 col-md-8 leading">
        <p>1 Локальная версия сайта предназначена для работы под Microsoft Windows.</p>
        <p>2 Для работы с локальной версии сайта требуется предварительная установка БД PostgreSQL 9.5, создание пользователя БД для ORM и создание пустой БД с кодировкой UTF-8 для хранения данных.</p>
        <p>3 После подготовки БД требуется запустить файл realhistory.exe, зайти по адресу <a href="http://127.0.0.1:8080">127.0.0.1:8080</a>, кликнуть по значку <span id="lnk_settings" class="lnk glyphicon glyphicon-cog"></span> и добавить реквизиты своей БД на сайт, например:
            <ul>
                <li>Host: 127.0.0.1</li>
                <li>DbName: gorm</li>
                <li>DbPwd: G%rmPwd777</li>
                <li>DbUser: gorm</li>
                <li>SslMode: disable</li>
            </ul>
        </p>
        <p>4 Для работы с сайтом требуется регистрация с использованием реального email, на который приходит ссылка для подтверждения регистрации. <strong>Данные по регистрациям не собираются</strong>. Все данные для авторизации сохраняются исключительно в Вашей локальной БД.
        </p>
        <p>5 Для запуска обучения на страницах <a class="emer_c" href="/events/xlsx">Таблица</a>, <a class="emer_c" href="/events/grid">Сетка</a> и <a class="emer_c" href="/events/flot">Графики</a> нажмите F2.
        </p>
	</div>
</div>