<div class="row">
	<div class="col-lg-offset-2 col-md-offset-2 col-lg-8 col-md-8 leading">
        <p>Исходный код сайта предоставляется по лицензии <a class="emer_hover" href="https://www.gnu.org/licenses/gpl-3.0.html">GPLv3</a></p>
        <p>В исходном коде сайта использованы следующие сторонние программные компоненты:
            <p>Сервер:</p>
            <ul class="emer_hover_cover">
                <li><a href="https://github.com/astaxie/beego">Beego Framework</a></li>
                <li><a href="https://github.com/jinzhu/gorm">gorm – ORM Library</a></li>
                <li><a href="https://github.com/jmoiron/sqlx">sqlx – General purpose extensions to golang's database/sql (ORM Library)</a></li>
                <li><a href="https://github.com/tealeg/xlsx">xlsx – golang library for reading and writing XLSX files</a></li>
                <li><a href="https://github.com/saintfish/chardet">chardet – Charset detector library for golang derived from ICU</a></li>
                <li><a href="https://github.com/deckarep/golang-set">golang-set – A simple set type for the Go language</a></li>   
            </ul>
            <p>Клиент:</p>
            <ul class="emer_hover_cover">
                <li><a href="https://jquery.com">jQuery 2</a></li>
                <li><a href="http://getbootstrap.com">Bootstrap 3</a></li>
                <li><a href="https://github.com/Eonasdan/bootstrap-datetimepicker">bootstrap-datetimepicker – Date/time picker widget based on twitter bootstrap</a></li>
                <li><a href="https://github.com/bevacqua/dragula">dragula – Drag'n Drop Library</a></li>
                <li><a href="https://datatables.net">DataTables – Table plugin for jQuery</a></li>
                <li><a href="https://github.com/superRaytin/paginationjs">paginationjs – A jQuery plugin to provide simple yet fully customisable pagination</a></li>
                <li><a href="https://github.com/ftlabs/ftdomdelegate">ftdomdelegate – DOM event delegator</a></li>
                <li><a href="https://github.com/fortesinformatica/Sideshow">Sideshow – javascript library which aims to reduce your user's learning curve by providing a way to create step-by-step interactive tours</a></li>
                <li><a href="https://github.com/flot/flot">flot – Attractive JavaScript charts for jQuery</a></li>
                <li><a href="http://jumflot.jumware.com/Experimental/Download.html">JUMFlot library and plugins</a></li>
                <li><a href="https://github.com/eligrey/FileSaver.js">FileSaver – An HTML5 saveAs() FileSaver implementation</a></li>
                <li><a href="https://github.com/ded/reqwest">reqwest – browser asynchronous http requests</a></li>
                <li><a href="http://specro.github.io/Philter/">philter – CSS filters with HTML attributes</a></li>  
                <li><a href="https://plus.google.com/108949996304093815163/about">dialogbox</a></li>
            </ul>
        </p>
	</div>
</div>