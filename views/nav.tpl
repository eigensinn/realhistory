<nav class="navbar navbar-default">
	<div class="row">
		<div class="nav_wrap col-md-offset-4 col-md-4">
			<ul class="nav navbar-nav">
				<li id="lnk_xlsx" class="nav-link"><a class="emer_hover" href="/events/xlsx">Таблица</a></li>
				<li class="divider-vertical"></li>
				<li id="lnk_grid" class="nav-link"><a class="emer_hover" href="/events/grid">Сетка</a></li>
				<li class="divider-vertical"></li>
				<li id="lnk_flot" class="nav-link"><a class="emer_hover" href="/events/flot">Графики</a></li>
				<li class="divider-vertical"></li>
				<li id="lnk_info" class="nav-link"><a class="emer_hover" href="/info">Информация</a></li>
			</ul>
		</div>
	</div>
</nav>