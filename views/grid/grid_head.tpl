<title>Сетка событий</title>
<link href="/static/css/grid.css" rel="stylesheet">
<link href="/static/css/dragula.css" rel="stylesheet">
<link href="/static/css/pagination.css" rel="stylesheet">
<link href="/static/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="/static/js/dragula.min.js"></script>
<script src="/static/js/dom-delegate.min.js"></script>
<script src="/static/js/pagination.min.js"></script>
<script src="/static/js/bootstrap-toggle.min.js"></script>
<script>
document.addEventListener('DOMContentLoaded', function() {
	url = {{.Url}};
	//Функция alert-сообщений
	function complete(data, cls) {
		var el = document.querySelector(cls);
		var span = document.createElement('span');
		el.appendChild(span);
		span.setAttribute("class", "data");
		span.innerHTML = data;
		el.classList.remove('fadeout');
		el.classList.add('fadein');
		setTimeout(function() {
			el.classList.remove('fadein');
			el.classList.add('fadeout');
		}, 3000);
		setTimeout(function() {
			var data = document.querySelector(".data");
			data.parentNode.removeChild(data);
		}, 5000);
	}
	
	
	function getXSRF() {
		var meta = document.getElementsByName("_xsrf")[0];
		return meta.getAttribute('content');
	}
	
	//document.getElementById('ny-btn').onclick = function() {
	var openDialog = function() {
		setDialog("open", {
			title: "Управление",
			width: 270,
			height: 400,
			content: "<div id='controls'><p data-toggle='tooltip' data-placement='bottom' title='При переходе на другую страницу'><input type='checkbox' id='btn-add' data-on='Добавлять' data-off='Заменять' data-onstyle='success' data-offstyle='danger'></p><p data-toggle='tooltip' data-placement='bottom' title='При добавлении новых данных'><input type='checkbox' id='btn-collect' data-on='Накапл. данн.' data-off='Стирать данн.' data-onstyle='success' data-offstyle='danger'></p><p><input type='checkbox' id='btn-edit' data-on='Правка' data-off='Просмотр' data-onstyle='success' data-offstyle='info'></p><p><button id='esc' type='button' class='btn btn-default btn-lg'><span class='glyphicon glyphicon-repeat' aria-hidden='true'></span> Отмена действия</button></p></div><div><div class='input-group'><span class='input-group-addon' id='quantity_lbl'>Количество</span><input type='text' id='quantity' class='form-control' readonly=readonly aria-describedby='quantity_lbl'></div><button id='btn-rem-data' type='button' class='btn btn-default btn-lg'><span class='glyphicon glyphicon-remove-circle' aria-hidden='true'></span> Удалить данные</button></div>"
		});
		$('#controls input').bootstrapToggle();
	}
	openDialog();
	$('[data-toggle="tooltip"]').tooltip()
	
	//Создание упорядоченного массива
	function UniqueSort(A)
	{
		var n = A.length, B = [A[0]];
		for (var i = 1, j = 1; i < n; i++) 
		 { if (A[ i ] !== B[j-1]) B[j++] = A[ i ]; }
		return B; 
	}

	//Получение элементов массива, имеющих дубли
	function getDublsSortArr(A)
	{   
		var n = A.length, B = [];
		for (var i = 1, j = 0; i < n; i++)
		 { if (A[i-1] === A[ i ]) B[j++] = A[i-1]; }
		 
		return UniqueSort(B);
	}
	
	function getIdx(el, source_or_target) {
		//Определяем уникальный идентификатор связанного события / Определяем уникальный идентификатор события, которое мы связываем
		var x_idx = el.classList[1];
		//Определяем id таблицы, в которой содержится связанное событие / Определяем id таблицы, в которой содержится связываемое событие
		var x_par_arr = el.classList[0].split("__");
		var x_par_id = parseInt(x_par_arr[x_par_arr.length - 1]);
		//Определяем уникальный идентификатор события / Определяем уникальный идентификатор события, с которым связываем
		var event_idx_arr = source_or_target.id.split("__");
		var event_idx = event_idx_arr[event_idx_arr.length - 1];
		//Определяем имя таблицы связанных событий (events, equals, rel_events или duplicates)
		var tbl_name = source_or_target.classList[0];
		var obj = {"tbl_name": tbl_name, "event_idx": event_idx, "x_par_id": x_par_id,"x_idx": x_idx};
		return obj;
	}
	
	function saveLast(obj) {
		reqwest({
			url: "http://{{.Url}}/savelast", type: 'json', method: 'post', contentType: 'application/json; charset=utf-8',
			headers: {'X-Xsrftoken': getXSRF()},
			data: JSON.stringify(obj),
			error: function (err) { complete(err, ".alert-danger"); }
		});
	}

	//Инициализируем dragula
	var drake = dragula({
		moves: function (el, container, handle) {
			return handle.classList.contains('handle');
		},
		copy: true,
		removeOnSpill: true
	}).on('drag', function (el) {
		complete("Для отмены нажмите Esc", ".alert-warning");
	}).on('drop', function (el, target) {
		//Определяем NodeList из детей target
		var contains = target.childNodes;
		//Преобразование NodeList в Array 
		var arr = Array.prototype.slice.call(contains);
		//Получение classList[0] из Array
		arr.forEach(function(item, i){
			arr[i] = item.classList[1];
		});
		//Если в массиве classList[0] содержится дубликат или если classList[0] совпадает c id target, удаляем текущий объект
		var dbl_id = getDublsSortArr(arr);
		var el_idx = el.classList[1];
		if (dbl_id != "" || el_idx == target.id.split("__")[1]) {
			drake.remove();
		//Если дубликата нет, делаем запрос
		} else {
			var obj = getIdx(el, target);
			//Определяем Id таблицы, в которой содержится событие, с которым связываем
			obj["event_par_id"] = parseInt(target.parentNode.previousElementSibling.childNodes[0].classList[0].split("__")[1]);
			//Проверка на совпадение таблиц при добавлении аналога
			if ((obj["tbl_name"] == "equals") && (obj["event_par_id"] == obj["x_par_id"])) {
				drake.remove();
				complete("Не могу добавить аналог из той же таблицы", ".alert-danger");
			} else {
				reqwest({
					url: "http://{{.Url}}/relinsert", type: 'json', method: 'post', contentType: 'application/json; charset=utf-8',
					headers: {'X-Xsrftoken': getXSRF()},
					data: JSON.stringify(obj),
					error: function (err) { complete(err, ".alert-danger"); },
					success: function (data) {
						//Удаляем старый класс
						var oldfix = el.classList[0].split("__")[0];
						var oldcolor = el.classList[2];
						var s = el.classList.toString();
						var old_classes = s.split(" ");
						var newfix;
						switch (obj.tbl_name) {
							case "rel_events":
								newfix = "rel_event"
								color = "bg-success"
								break;
							case "duplicates":
								newfix = "dup";
								color = "bg-danger"
								break;
							case "equals":
								newfix = "eq";
								color = "bg-warning"
								break;
						}
						var new_s = s.replace(oldfix, newfix);
						var classes = new_s.replace(oldcolor, color).split(" ");
						old_classes.forEach(function(cls, i){
							el.classList.remove(cls);
						});
						classes.forEach(function(cls, i){
							el.classList.add(cls);
						});
						complete(data, ".alert-success");
						obj.type = "insert";
						saveLast(obj);
					}
				});
			}
		}
	}).on('cancel', function (el, container, source) {
		//Если мы выбраcываем событие за пределы виджетов
		if (container == null){
			var obj = getIdx(el, source);
			reqwest({
				url: "http://{{.Url}}/eventremove", type: 'json', method: 'post', contentType: 'application/json; charset=utf-8',
				headers: {'X-Xsrftoken': getXSRF()},
				data: JSON.stringify(obj),
				error: function (err) { complete(err, ".alert-danger"); },
				success: function (data) {
					complete(data, ".alert-success");
					//Удаляем автоматически создаваемую копию элемента
					var selector = source.children;
					Array.prototype.forEach.call(selector, function(item, i){
						if (item.classList[0] == el.classList[0] && item.classList[1] == el.classList[1]) {
							source.removeChild(item);
						}
					});
					if(source.classList.contains("events")) {
						obj.event_par_id = obj.x_par_id;
					} else {
						obj.event_par_id = parseInt(source.parentNode.previousElementSibling.childNodes[0].classList[0].split("__")[1]);
					}
					if (obj.event_idx == "") {
						obj.event_idx = obj.x_idx;
					}
					//Если удалено событие, удаляем виджет из DOM
					if (obj.tbl_name == "events") {
						var removeflag = source.parentNode;
						removeflag.classList.add('fadeout');
						setTimeout(function() {
							removeflag.parentNode.removeChild(removeflag);
						}, 450);
					}
					obj.type = "remove";
					saveLast(obj);
				}
			});
		}
	});
	
	var current = {};
	var global_data = [];
	//Имена таблиц
	var tbl_name = {{.tblNames}};
	var event_type = {{.eventTypes}};
	//Добавление options (list) в select (sel)
	function addOptions(list, sel) {
		if (list != null) {
			list.forEach(function(txt, i){
				var opt = document.createElement('option');
				sel.appendChild(opt);
				opt.innerHTML = txt;
				opt.value = txt;
			});
		}
	}
	var sel = document.getElementById("tblname");
	var sel2 = document.getElementById("eventtype");
	addOptions(tbl_name, sel);
	addOptions(event_type, sel2);

	//Добавление виджетов в pagination
	function append(data) {
		var column = document.createElement('div');
		column.classList.add("column");
		var dnd = [];
		for(var key in data) {
			if (data.hasOwnProperty(key)) {
				var val = data[key];
				if (val.Idx != undefined) {
					var item = document.createElement('div');
					column.appendChild(item);
					var h = '<div class="panel panel-primary col-xs-12 col-sm-6 col-md-4 col-lg-3">';
					h += '<div class="events panel-heading"><span class="event__'+val.HId+' '+val.Idx+' bg-primary" id="'+val.Idx+'">'+val.HId+': '+val.Idx+'<span class="handle hide-handle"></span></span><button type="button" data-toggle="modal" data-target="#rem_evnt_mod" class="rem_evnt rem_evnt_btn">×</button></div>';
					h += '<div class="panel-body pbody-closed">';
					
					/*h += '<p>Название: '+val.Name+'</p>';
					h += '<p>Дата: '+val.Start+' – '+val.End+'</p>';
					h += '<p>Регион: '+val.Region+'</p>';
					h += '<p class="bg-info">'+val.Description+'</p>';*/
					h += '<p>Описание:</p>';
					h += '<div class="bg-info event-info pbody-closed">';
					for(var k in val) {
						if (val.hasOwnProperty(k)) {
							var v = val[k];
							h += '<p>'+k+': '+v+'</p>';
						}
					}
					h += '</div>';
					h += '<div class="expand"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></div>';
					h += '<p class="analogs">Аналоги в таблицах:</p>';
					var eq = "equals__"+val.Idx;
					h += '<div class="equals" id="'+eq+'">';
					h += '</div>';
					h += '<p>Связанные события:</p>';
					var reid = "rel_events__"+val.Idx;
					h += '<div class="rel_events" id="'+reid+'">';
					h += '</div>';
					h += '<p>Исторические дубликаты:</p>';
					var dup = "duplicates__"+val.Idx;
					h += '<div class="duplicates" id="'+dup+'">';
					h += '</div>';
					h += '</div>';
					h += '<div class="expand"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></div>';
					h += '</div>';
					item.outerHTML = h;
					dnd.push(eq);
					dnd.push(reid);
					dnd.push(dup);
				}
			}
		}
		var grid = document.getElementById("grid");
		var btn_add = document.getElementById("btn-add");
		if (btn_add.parentNode.classList.contains("off")) {
			//Заменяем элементы
			grid.innerHTML = "";
		}
		//Добавляем элементы
		grid.appendChild(column);

		//Добавляем связанным событиям свойство drag&drop
		dnd.forEach(function(item, i){
			var container = document.getElementById(item);
			drake.containers.push(container);
		});
		for(var key in data) {
		   if (data.hasOwnProperty(key)) {
			   var val = data[key];
				if (val.Idx == undefined && val.EqIdx != undefined) {
					var eq_cont = new RelContainer(val, "equals__", "eq__", "bg-warning", "EventIdx");
					eq_cont.append("EqIdx", "EqParId");
				}
			}
		}
		for(var key in data) {
		   if (data.hasOwnProperty(key)) {
			   var val = data[key];
				if (val.Idx == undefined && val.RelIdx != undefined) {
					var rel_cont = new RelContainer(val, "rel_events__", "rel_event__", "bg-success", "EventIdx");
					rel_cont.append("RelIdx", "RelParId");
				}
			}
		}
		for(var key in data) {
		   if (data.hasOwnProperty(key)) {
			   var val = data[key];
				if (val.Idx == undefined && val.DupIdx != undefined) {
					var dup_cont = new RelContainer(val, "duplicates__", "dup__", "bg-danger", "EventIdx");
					dup_cont.append("DupIdx", "DupParId");
				}
			}
		}
		//Добавляем заголовкам свойство drag&drop
		var headings = document.querySelectorAll(".panel-heading");
		Array.prototype.forEach.call(headings, function(el, i){
			drake.containers.push(el);
		});
	}
	
	//Контейнер событий со значениями, префиксами и цветом
	function RelContainer(val, cont_prefix, span_prefix, color, eidx){
		this.val = val;
		var cont_id = cont_prefix + this.val[eidx];
		this.events = document.getElementById(cont_id);
		this.span_prefix = span_prefix;
		this.color = color;
	}
	
	//Отображение виджетов внутри контейнера
	RelContainer.prototype.append = function(XIdx, XParId) {
		var p = this;
		if (this.val[XIdx] != "") {
			var span = document.createElement('span');
			if (p.events != null) {
				p.events.appendChild(span);
				span.classList.add(p.span_prefix+p.val[XParId], p.val[XIdx], p.color);
				
				var txt = document.createElement('span');
				txt.classList.add("small", "info");
				span.appendChild(txt);
				span.setAttribute("data-target", "#info_mod");
				span.setAttribute("data-toggle", "modal");
				txt.innerHTML = p.val[XParId] + ": " + p.val[XIdx];
				
				var handle = document.createElement('span');
				handle.classList.add("handle");
				span.appendChild(handle);
				var toggle = document.getElementById("btn-edit");
				if (!toggle.parentNode.classList.contains("off")) {
					handle.style.borderTopWidth = "35px";
					handle.style.borderRightWidth = "35px";
				}
			}
		}
	}

	function showWidgets() {
		var columns = document.querySelectorAll(".column");
		Array.prototype.forEach.call(columns, function(el, i){
			el.innerHTML = '';
		});
		var el_type = document.getElementById("eventtype");
		var event_type = el_type.options[el_type.selectedIndex].value;
		var el_tbl = document.getElementById("tblname");
		var tbl_name = el_tbl.options[el_tbl.selectedIndex].value;
		
		$('#pagination-container').pagination({
			dataSource: function(done) {
				$.ajax({
					type: 'POST',
					url: "http://{{.Url}}/appendtogrid",
					headers: {'X-Xsrftoken': getXSRF()},
					data: JSON.stringify({"tbl_name": tbl_name, "event_type": event_type}),
					contentType: 'application/json; charset=utf-8',
					success: function(response) {
						var btn_collect = document.getElementById("btn-collect");
						if (btn_collect.parentNode.classList.contains("off")) {
							done(response);
						} else {
							global_data = global_data.concat(response);
							document.getElementById('quantity').value = String(global_data.length);
							done(global_data);
						}
					}
				});
			},
			callback: function(data, pagination) {
				append(data);
			}
		});
	}
	
	//Замена лейбла на кнопке для загрузки данных
	$('#btn-collect').change(function() {
		var btn_show = document.getElementById("btn-show");
		if (this.parentNode.classList.contains("off")) {
			btn_show.innerHTML = '<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Показать';
		} else {
			btn_show.innerHTML = '<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Добавить';
		}
    })
	
	//Удаление накопленных данных
	var btn_rem_data = document.getElementById('btn-rem-data');
	btn_rem_data.onclick = function() {
		document.getElementById('quantity').value = "0";
		global_data = [];
	}
	
	//Просмотр/Правка
	$('#btn-edit').change(function() {
		var selector = document.querySelectorAll(".handle");
		if (this.parentNode.classList.contains("off")) {
			Array.prototype.forEach.call(selector, function(item, i){
				item.style.borderTopWidth = "0px";
				item.style.borderRightWidth = "0px";
			});
		} else {
			Array.prototype.forEach.call(selector, function(item, i){
				item.style.borderTopWidth = "35px";
				item.style.borderRightWidth = "35px";
			});
		}
	});
	
	function Cancel() {
		reqwest({
			url: "http://{{.Url}}/cancel", method: 'post',
			headers: {'X-Xsrftoken': getXSRF()},
			error: function (err) { complete(err, ".alert-danger"); },
			success: function (data) {
				if (data.type != "events") {
					var msg;
					var word;
					switch (data.type) {
						case "insert":
							var span;
							var prefix;
							var id = data.tbl_name + "__" + String(data.event_idx);
							var rel_cont = document.getElementById(id);
							//Потому что если class начинается с цифры, querySelector не работает
							var spans = rel_cont.getElementsByClassName(data.x_idx);
							switch (data.tbl_name) {
								case "rel_events":
									prefix = "rel_event__"
									word = "связанного события ";
									break;
								case "duplicates":
									var prefix = "dup__";
									word = "дубликата ";
									break;
								case "equals":
									var prefix = "eq__";
									word = "аналога ";
									break;
							}
							Array.prototype.forEach.call(spans, function(item, i){
								if (item.classList.contains(prefix + String(data.x_par_id))) {
									span = item;
								}
							});
							span.classList.add('fadeout');
							setTimeout(function() {
								rel_cont.removeChild(span);
							}, 450);
							msg = "Отменено добавление " + word + data.x_idx;
							break;
						case "remove":
							var word2;
							var rel_cont;
							switch (data.tbl_name) {
								case "rel_events":
									rel_cont = new RelContainer(data, "rel_events__", "rel_event__", "bg-success", "event_idx");
									word = "Связанное событие ";
									word2 = " возвращено ";
									break;
								case "duplicates":
									rel_cont = new RelContainer(data, "duplicates__", "dup__", "bg-danger", "event_idx");
									word = "Дубликат ";
									word2 = " возвращен ";
									break;
								case "equals":
									rel_cont = new RelContainer(data, "equals__", "eq__", "bg-warning", "event_idx");
									word = "Аналог ";
									word2 = " возвращен ";
									break;
							}
							rel_cont.append("x_idx", "x_par_id");
							msg = word + data.x_idx + word2 + " к событию " + data.event_idx;
							break;
					}	
					complete(msg, ".alert-warning");
				}
			}
		});
	}
	
	$(document).keyup(function(e) {
		if (e.keyCode === 27) {
			//Если нажата клавиша esc, вставляем событие обратно после удаления или удаляем добавленное связанное событие
			Cancel();
		}
	});
	
	var btn_esc = document.getElementById('esc');
	btn_esc.onclick = function() {
		Cancel();
	}

	//Демонстрация выборки
	var btn_show = document.getElementById('btn-show');
	btn_show.onclick = function() {
		showWidgets();
	}

	//Демонстрация модального окна для удаления события
	function removeEvent() {
		//Выбираем элемент из заголовка и считываем его id
		var el = this.previousElementSibling;
		current.idx = el.id;
		current.tbl = parseInt(el.classList[0].split("__")[1]);
		//Выбираем modal-body
		var mod_body = document.querySelectorAll(".modal-body")[0];
		//Формируем новую строку
		var new_txt = "Вы действительно хотите удалить событие " + current.idx + "?";
		//Вставляем строку в modal-body
		mod_body.innerHTML = new_txt;
	}
	
	//Функция демонстрации описания
	function toggleDescription() {
		var pbody = this.previousElementSibling;
		var span = this.children[0];
		if (span.classList.contains("glyphicon-chevron-down")) {
			pbody.classList.remove('pbody-closed');
			pbody.classList.add('pbody-opened');
			span.classList.remove('glyphicon-chevron-down');
			span.classList.add('glyphicon-chevron-up');
		} else {
			pbody.classList.remove('pbody-opened');
			pbody.classList.add('pbody-closed');
			span.classList.remove('glyphicon-chevron-up');
			span.classList.add('glyphicon-chevron-down');
		}
	}
	
	//Получение информации о связанном событии
	function getInfo() {
		var classes = this.parentNode.classList;
		var x_par_id = parseInt(classes[0].split("__")[1]);
		var x_idx = classes[1];
		reqwest({
			url: "http://{{.Url}}/getinfo",
			type: 'json',
			method: 'post',
			contentType: 'application/json; charset=utf-8',
			headers: {'X-Xsrftoken': getXSRF()},
			data: JSON.stringify({"x_par_id": x_par_id, "x_idx": x_idx}),
			error: function (err) { complete(err, ".alert-danger"); },
			success: function (data) {
				var title = document.getElementById("info_mod_lbl");
				title.innerHTML = "Просмотр свойств события " + x_idx;
				var body = document.querySelector("#info_mod .modal-body");
				body.innerHTML = "";
				for(var k in data[0]) {
					if (data[0].hasOwnProperty(k)) {
						var v = data[0][k];
						var p = document.createElement("p");
						p.innerHTML = k+': '+v;
						body.appendChild(p);
					}
				}
			}
		});
	}

	var grid = document.getElementById("grid");
	var delegate = domDelegate(grid);
	delegate.on('click', '.expand', toggleDescription);
	delegate.on('click', '.rem_evnt_btn', removeEvent);
	delegate.on('click', '.info', getInfo);
	
	//Запрос при нажатии на ОК
	var allow = document.getElementById('allow');
	allow.onclick = function() {
		reqwest({
			url: "http://{{.Url}}/eventremove",
			type: 'json',
			method: 'post',
			contentType: 'application/json; charset=utf-8',
			headers: {'X-Xsrftoken': getXSRF()},
			data: JSON.stringify({"tbl_name": "events", "x_par_id": current.tbl, "x_idx": current.idx}),
			error: function (err) { complete(err, ".alert-danger"); },
			success: function (data) {
				complete(data, ".alert-success");
				showWidgets();
			}
		});
	}
});
</script>