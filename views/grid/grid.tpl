<div class="row">
	<div class="col-md-6 col-lg-6" id="table-change">
		<p>Выбор таблицы
			<select name="tblname" id="tblname"></select>
		</p>
		<p>Выбор типа события
			<select name="eventtype" id="eventtype">
				<option value="*">*</option>
			</select>
		</p>
		<button id="btn-show" type="button" class="btn btn-default btn-lg">
			<span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> Показать
		</button>
	</div>
	<div id="pagination-container" class="col-md-12 col-lg-12"></div>
	<h1 class="col-xs-12 col-sm-12 col-md-12 col-lg-12">Данные по типу события</h1>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 row" id="grid" data-columns></div>
</div>
<div class="modal fade" id="rem_evnt_mod" tabindex="-1" role="dialog" aria-labelledby="rem_evnt_mod_lbl">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="rem_evnt" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="rem_evnt_mod_lbl">Подтверждение удаления</h4>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
				<button type="button" id="allow" class="btn btn-primary" data-dismiss="modal">ОК</button>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="info_mod" tabindex="-1" role="dialog" aria-labelledby="info_mod_lbl">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="rem_evnt" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="info_mod_lbl"></h4>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
			</div>
		</div>
	</div>
</div>