<title>Таблицы событий</title>
<link href="/static/css/jquery.fileupload.css" rel="stylesheet">
<link href="/static/css/jquery.dataTables.css" rel="stylesheet">
<link href="/static/css/dragula.css" rel="stylesheet">
<script src="/static/js/FileSaver.min.js"</script>
<script src="/static/js/jquery.ui.widget.js"></script>
<script src="/static/js/jquery.dataTables.min.js"></script>
<script src="/static/js/dragula.min.js"></script>
<script src="/static/js/dom-delegate.min.js"></script>
<script>
document.addEventListener('DOMContentLoaded', function() {
	url = {{.Url}};
	//Индикатор окончания загрузки для обучения
	upload_complete = 1;
	//Имена таблиц
	var tbl_names = {{.tblNames}};
	//Добавление options (list) в select (sel)
	function addOptions(list, sel) {
		if (list != null) {
			list.forEach(function(txt, i){
				var opt = document.createElement('option');
				sel.appendChild(opt);
				opt.innerHTML = txt;
				opt.value = txt;
			});
		}
	}
	var sel = document.getElementById("tbl_names");
	addOptions(tbl_names, sel);
	//Функция alert-сообщений
	function complete(data, cls) {
		var el = document.querySelector(cls);
		var span = document.createElement('span');
		el.appendChild(span);
		span.setAttribute("class", "data");
		span.innerHTML = data;
		el.classList.remove('fadeout');
		el.classList.add('fadein');
		setTimeout(function() {
			el.classList.remove('fadein');
			el.classList.add('fadeout');
		}, 3000);
		setTimeout(function() {
			var data = document.querySelector(".data");
			data.parentNode.removeChild(data);
		}, 3500);
	}
	//Функция сериализации формы
	$.fn.serializeFormJSON = function() {
	   var o = {};
	   var a = this.serializeArray();
	   a.forEach(function(el, i){
		   if (o[el.name]) {
			   if (!o[el.name].push) {
				   o[el.name] = [o[el.name]];
			   }
			   o[el.name].push(el.value || '');
		   } else {
			   o[el.name] = el.value || '';
		   }
	   });
	   return o;
	};
	
	function getXSRF() {
		var meta = document.getElementsByName("_xsrf")[0];
		return meta.getAttribute('content');
	}
	
	var openDialog = function() {
		setDialog("open", {
			title: "Управление",
			width: 270,
			height: 400,
			content: "<button id='btn_add_row' type='button' class='tbl btn btn-default btn-lg'><span class='glyphicon glyphicon-plus-sign'></span>Добавить строку</button><button id='btn_rem_row' type='button' class='tbl btn btn-default btn-lg'><span class='glyphicon glyphicon-minus-sign'></span>Удалить строку</button><button id='btn_save' type='button' class='tbl btn btn-default btn-lg'><span class='glyphicon glyphicon-save admin-save'></span>Сохранить</button>"
		});
	}
	openDialog();

	//Запрос при выборе имени таблицы
	$( "select#tbl_names" ).click(function () {
		if ( $(this).data('clicks') == 1 ) {
			var el = document.getElementById("tbl_names");
			var val = el.options[el.selectedIndex].value;
			//Делаем запрос строки, содержащей заголовки
			reqwest({
				url: "http://{{.Url}}/heads",
				method: 'post',
				headers: {'X-Xsrftoken': getXSRF()},
				data: {"table": val},
				success: function(data){
					//Вставляем заголовки в таблицу
					var arr = data["Heads"].split(',');
					var columns = [];
					var heads = document.getElementById("heads");
					heads.innerHTML = '';
					arr.forEach(function(value, i){
						columns.push({"data": value, "defaultContent": "<i>–</i>"})
						var th = document.createElement('th');
						th.innerHTML = value;
						heads.appendChild(th);
					});
					//Загружаем данные в таблицу соответствий
					var names = ["Idx","Name","Description","Start","End","Region","EventType","ShiftDate"];
					names.forEach(function(value, i){
						var parent = document.getElementsByName(value)[0];
						parent.innerHTML = "";
						var th = document.createElement('th');
						parent.appendChild(th);
						th.innerHTML = data[value];
					});
					var table = $('#tbl').DataTable({
						retrieve: true
					});
					table.destroy();
					//Делаем запрос для получения данных таблицы
					var table = $('#tbl').DataTable({
						retrieve: true,
						language: {
							url: '/static/json/Russian.json'
						},
						"ajax": {
							"url": "http://{{.Url}}/fill",
							"type": "POST",
							"headers": {'X-Xsrftoken': getXSRF()},
							"dataSrc": "",
							"data": {"table": val}
						},
						"columns": columns
					});
				},
				error: function(err) {
					complete(err, ".alert-danger");
				}
			});
			$(this).data('clicks', 0);
		} else {
			$(this).data('clicks', 1);
		}
	});
	
	$( "select#tbl_names" ).focusout( function() {
		$(this).data('clicks', 0);
	});
	
	//Удаление строки таблицы
	var btn_rem_row = document.getElementById('btn_rem_row');
	btn_rem_row.onclick = function() {
		var el = document.getElementById("tbl_names");
		var tbl = el.options[el.selectedIndex].value;
		var cls = this.getAttribute("class").split(" ");
		var col = $('#'+cls[0]+' thead #heads th').first().text();
		var table = $('#tbl').DataTable();
		var jsonstring = JSON.stringify({"rh_tbl": tbl, "rh_col": col, "rh_row": $('#'+cls[0]+' tbody tr.selected td.sorting_1').text()});
		reqwest({
			url: "http://{{.Url}}/removerowact",
			type: 'json',
			method: 'post',
			contentType: 'application/json; charset=utf-8',
			headers: {'X-Xsrftoken': getXSRF()},
			data: jsonstring,
			success: function(data){
				table.row('.selected').remove().draw( false );
				complete(data, ".alert-success");
			},
			error: function() {
				complete("Ошибка!", ".alert-danger");
			}
		});
	}

	//Определяет, содержит ли передаваемый объект какие-то данные кроме идентификатора строки row
	Object.size = function(obj) {
		var size = 0, key;
		for (key in obj) {
			if (obj.hasOwnProperty(key)) size++;
		}
		return size;
	};

	//Передает новые данные в базу данных
	var btn_save = document.getElementById('btn_save');
	btn_save.onclick = function() {
		var selector = document.querySelectorAll('#tbl tbody tr');
		Array.prototype.forEach.call(selector, function(item, i){
			var el = document.getElementById("tbl_names");
			var tbl = el.options[el.selectedIndex].value;
			var corr_i = String(i+1);
			var col = $('#tbl'+' thead #heads th').first().text();
			var row = $('#tbl tr:eq(' + corr_i + ') td.sorting_1').text();
			var params = {"rh_tbl": tbl, "rh_col": col, "rh_row": row};
			var new_data = {};
			var inner_selector = item.childNodes;
			Array.prototype.forEach.call(inner_selector, function(inner_item, j){
				if (inner_item.classList.contains("updated")) {
					var cls = inner_item.getAttribute("class").split(' ');
					//Если это не первый столбец
					if (cls[1] != "updated") {
						var iD = cls[1].split('_');
						var text = inner_item.textContent;
						//Определяем объекты с именами полей по заголовкам таблицы
						new_data[$('#tbl thead #heads th:eq(' + iD[2] + ')').text()] = text;
					}
				}
			});
			var size = Object.size(new_data);
			//Если есть какие-то данные в new_data, отправляем
			if (size > 0) {
				reqwest({
					url: "http://{{.Url}}/saveact",
					type: 'json',
					method: 'post',
					contentType: 'application/json; charset=utf-8',
					headers: {'X-Xsrftoken': getXSRF()},
					data: JSON.stringify({"params": params, "new_data": new_data}),
					success: function(data){
						complete(data, ".alert-success");
					},
					error: function() {
						complete("Ошибка!", ".alert-danger");
					}
				});
			}
		});
	};
	
	//Добавление новой строки, заполненной символами "–"
	function addNewRow() {
		var t = $('#tbl').DataTable();
		var empt = [];
		for (i = 0; i < $("#tbl td").length; i++) {
			empt.push("");
		}
		t.row.add( empt ).draw( false );
	}
	
	//Выделение строки таблицы
	function selectRow() {
		if ( this.classList.contains('selected') ) {
			this.classList.remove('selected');
		}
		else {
			var siblings = Array.prototype.filter.call(this.parentNode.children, function(child){return child !== this;});
			siblings.forEach(function(el, i){
				el.classList.remove('selected');
			});
			this.classList.add('selected');
		}
	}
	
	//Создает временное поле input для ввода нового значения по двойному клику
	function addDynamicField() {
		var val = this.textContent
		var col = $(this).parent().children().index($(this));
		var row = $(this).parent().parent().children().index($(this).parent());
		var iD = "cell_"+row+"_"+col
		this.innerHTML = '<input class="new_input" id="'+iD+'" type="text" value="'+val+'"></input>';
	};
	
	//Передача значения из временного поля input в ячейку таблицы по событию blur (смена фокуса)
	function sendValueToCell() {
		var iD = this.getAttribute("id").split("_");
		var corr_row = String(parseInt(iD[1])+1);//Почему-то приходится прибавлять 1 для eq
		var td = $('#tbl tr:eq(' + corr_row + ') td:eq(' + iD[2] + ')').get(0);
		td.innerHTML = this.value;
		td.classList.add('updated');
		td.classList.add(this.getAttribute("id"));
	};
	
	var wrap = document.querySelector(".tbl_wrap");
	var delegate = domDelegate(wrap);
	var dc = document.querySelector(".dialog-content");
	var delegate2 = domDelegate(dc);
	delegate2.on('click', '#btn_add_row', addNewRow);
	delegate.on('click', '#tbl tbody tr', selectRow);
	delegate.on('dblclick', '#tbl tbody tr td', addDynamicField);
	delegate.on('blur', '.new_input', sendValueToCell);
	
	var curr_source;

	//Передача имен колонок года, региона, типа события и даты сдвига на сервер
	dragula([document.getElementById("idx"), document.getElementById("name"),
			document.getElementById("description"), document.getElementById("heads"),
			document.getElementById("start"), document.getElementById("end"),
			document.getElementById("region"), document.getElementById("event_type"),
			document.getElementById("shift_date")], {
		copy: function (el, source) {
			return source === document.getElementById("heads")
		},
		accepts: function (el, target) {
			return target !== document.getElementById("heads")
		},
			removeOnSpill: true
		}).on('drop', function (el, target) {
			//Если в целевой ячейке уже есть элемент, удаляем его
			if (target.childNodes.length > 1) {
				var rem_el = target.firstChild;
				rem_el.parentNode.removeChild(rem_el);
			}
			var col = {};
			col[target.id] = el.innerHTML;
			var el = document.getElementById("tbl_names");
			var tbl_name = el.options[el.selectedIndex].value;
			col["tbl_name"] = tbl_name;
			reqwest({
				url: "http://{{.Url}}/sendcol",
				type: 'json',
				method: 'post',
				contentType: 'application/json; charset=utf-8',
				headers: {'X-Xsrftoken': getXSRF()},
				data: JSON.stringify(col),
				success: function(data){
					complete(data, ".alert-success");
				},
				error: function() {
					complete("Ошибка!", ".alert-danger");
				}
			});
		}).on('remove', function (el, container) {
			var col = {};
			col[curr_source] = "None";
			var el = document.getElementById("tbl_names");
			var tbl_name = el.options[el.selectedIndex].value;
			col["tbl_name"] = tbl_name;
			reqwest({
				url: "http://{{.Url}}/sendcol",
				type: 'json',
				method: 'post',
				contentType: 'application/json; charset=utf-8',
				headers: {'X-Xsrftoken': getXSRF()},
				data: JSON.stringify(col),
				success: function(data){
					complete(data, ".alert-success");
				},
				error: function() {
					complete("Ошибка!", ".alert-danger");
					
				}
			});
		}).on('drag', function (el, source) {
			curr_source = source.id;
		});
	
	//Проверка input-полей
	$( "#fm_param > input" ).change(function(){
		var val = this.value;
		if(Number(val) === parseInt(val, 10)) {
			var col_names = document.getElementById("col_names");
			var data_row = document.getElementById("data_row");
			//Если обе сроки правильно заполнены, но значение в col_names больше, чем в data_row
			if (parseInt(col_names.value) > parseInt(data_row.value)) {
				this.value = "";
			}
		//Если значение - не число
		} else {
			this.value = "";
		}
	});
});
</script>