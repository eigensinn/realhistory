<div class="row">
	<div class="col-lg-6 col-md-6">
		<form id="fm_param">
			<input name="col_names" id="col_names" type="text" value=""><span>Строка имен столбцов</span><br>
			<input name="data_row" id="data_row" type="text" value=""><span>Первая строка данных</span><br>
		</form>
		<div id="table_choice">
			<div>Выбор таблицы</div>
			<select name="tbl_names" id="tbl_names">
			</select>
		</div>
	</div>
	<div class="col-lg-6 col-md-6" id="upload_wrap">
		<div id="progress2" class="progress">
			<div class="progress-bar progress-bar-success">Загрузка файла / Конвертация в SQL</div>
		</div>
		<div id="files" class="files"></div>
		<span class="btn btn-success fileinput-button">
			<i class="glyphicon glyphicon-plus"></i>
			<span>Загрузить файл...</span>
			<!-- The file input field used as target for the file upload widget -->
            <input id="uploadfile" type="file" name="uploadfile" onchange="handleFiles(this.files)"> 
		</span>
        <label id="lbl_for_upload"></label>
	</div>
	<div id="tables" class="col-md-offset-1 col-lg-offset-1 col-lg-10 col-md-10 row">
		<div class="col-lg-3 col-md-3"><div class="clmn clmn_0 small">Id</div><div id="idx" name="Idx" class="clmn clmn_1"></div></div>
		<div class="col-lg-3 col-md-3"><div class="clmn clmn_0 small">Имя</div><div id="name" name="Name" class="clmn clmn_1"></div></div>
		<div class="col-lg-3 col-md-3"><div class="clmn clmn_0 small">Описание</div><div id="description" name="Description" class="clmn clmn_1"></div></div>
		<div class="col-lg-3 col-md-3"><div class="clmn clmn_0 small">Начало</div><div id="start" name="Start" class="clmn clmn_1"></div></div>
		<div class="col-lg-3 col-md-3"><div class="clmn clmn_0 small">Окончание</div><div id="end" name="End" class="clmn clmn_1"></div></div>
		<div class="col-lg-3 col-md-3"><div class="clmn clmn_0 small">Регион</div><div id="region" name="Region" class="clmn clmn_1"></div></div>
		<div class="col-lg-3 col-md-3"><div class="clmn clmn_0 small">Тип события</div><div id="event_type" name="EventType" class="clmn clmn_1"></div></div>
		<div class="col-lg-3 col-md-3"><div class="clmn clmn_0 small">Дата сдвига</div><div id="shift_date" name="ShiftDate" class="clmn clmn_1"></div></div>
		<div class="tbl_wrap col-lg-12 col-md-12">
			<table id="tbl">
				<thead>
					<tr id="heads"></tr>
				</thead>
			</table>
		</div>
	</div>
</div>
<script>
function getXSRF() {
    var meta = document.getElementsByName("_xsrf")[0];
    return meta.getAttribute('content');
}
//Функция alert-сообщений
function complete(data, cls) {
    var el = document.querySelector(cls);
    var span = document.createElement('span');
    el.appendChild(span);
    span.setAttribute("class", "data");
    span.innerHTML = data;
    el.classList.remove('fadeout');
    el.classList.add('fadein');
    setTimeout(function() {
        el.classList.remove('fadein');
        el.classList.add('fadeout');
    }, 3000);
    setTimeout(function() {
        var data = document.querySelector(".data");
        data.parentNode.removeChild(data);
    }, 3500);
}
function handleFiles(files) {
    var col_names = document.getElementById("col_names").value;
    var data_row = document.getElementById("data_row").value;
    var blob = new Blob([col_names+"_"+data_row], {type: "text/plain;charset=utf-8"});
    var formdata = new FormData();
    formdata.append('file-0', files[0]);
    formdata.append('file-1', blob);
    
    if (col_names == "" || data_row == "" ) {
        complete("Не все поля заполнены!", ".alert-danger");
    } else {
        $.ajax({
            xhr: function () {
                //Прогресс конвертации в SQL (загрузка на сервер)
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        var pb = document.querySelector('#progress2 .progress-bar');
                        pb.style.width = '30%';
                    }
               }, false);
                //Прогресс конвертации в SQL (получение данных с сервера)
                xhr.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var pb = document.querySelector('#progress2 .progress-bar');
                        pb.style.width = parseInt(evt.loaded / evt.total * 100, 10) + '%';
                    }
                }, false);
                return xhr;
            },
            url: "http://{{.Url}}/xlsxtosql",
            type: 'POST',
            contentType: false,
            cache: false,
            processData: false,
            headers: {'X-Xsrftoken': getXSRF()},
            data: formdata,
            success: function(data){
                var msg = 'Создана таблица '+data;
                complete(msg, ".alert-success");
                var opt = document.createElement('option');
                var sel = document.getElementById("tbl_names");
                sel.appendChild(opt);
                opt.innerHTML = data;
                opt.value = data;
                //Сказали обучению, что загрузка завершена
                upload_complete = 1;
            },
            error: function(err) {
                complete(err, ".alert-danger");
            }
        });
    }
    document.getElementById("lbl_for_upload").innerHTML = files[0].name;
}
</script>