<div id="cont" class="row">
    <div class="col-md-12 col-lg-12 row" id="table-change">
        <div class="col-md-6 col-lg-6">
            <div>Выбор таблицы</div>
            <select name="tblname" id="tblname">
            </select>
        </div>
        <div class="col-md-6 col-lg-6">
            <div>Выбор типа события</div>
            <select name="eventtype" id="eventtype">
            </select>
        </div>
    </div>
    <div class="col-md-12 col-lg-12 row" id="date-change">
        <div class="col-md-6 col-lg-6">
            <div class="form-group">
                <div class='input-group date datetimepicker'>
                    <input id="start" type='text' class="form-control">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar">
                        </span>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-6">
            <div class="form-group">
                <div class='input-group date datetimepicker'>
                    <input id="end" type='text' class="form-control">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar">
                        </span>
                    </span>
                </div>
            </div>
        </div>
    </div>
	<div class="col-md-12 col-lg-12" id="touchflot"></div>
	<div class="col-md-12 col-lg-12" id="legendcontainer"></div>
</div>