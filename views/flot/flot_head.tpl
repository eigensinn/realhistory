<title>Графики событий</title>
<link href="/static/img/favicon.png" type="image/png" rel="icon">
<link href="/static/css/bootstrap-datetimepicker.css" rel="stylesheet">
<link href="/static/css/flot.css" rel="stylesheet">
<link href="/static/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="/static/js/flot/jquery.flot.js"></script>
<script src="/static/js/flot/jquery.flot.JUMlib.js"></script>
<script src="/static/js/flot/jquery.flot.mouse.js"></script>
<script src="/static/js/flot/jquery.flot.gantt.js"></script>
<script src="/static/js/moment.min.js"></script>
<script src="/static/js/bootstrap-datetimepicker.min.js"></script>
<script src="/static/js/bootstrap-toggle.min.js"></script>
<script>
document.addEventListener('DOMContentLoaded', function() {
	url = {{.Url}};
	//Имена таблиц
	var tbl_name = {{.tblNames}};
	var event_type = {{.eventTypes}};
	//Добавление options (list) в select (sel)
	function addOptions(list, sel) {
		if (list != null) {
			list.forEach(function(txt, i){
				var opt = document.createElement('option');
				sel.appendChild(opt);
				opt.innerHTML = txt;
				opt.value = txt;
			});
		}
	}
	var sel = document.getElementById("tblname");
	var sel2 = document.getElementById("eventtype");
	addOptions(tbl_name, sel);
	addOptions(event_type, sel2);
	
	//Функция alert-сообщений
	function complete(data, cls) {
		var el = document.querySelector(cls);
		var span = document.createElement('span');
		el.appendChild(span);
		span.setAttribute("class", "data");
		span.innerHTML = data;
		el.classList.remove('fadeout');
		el.classList.add('fadein');
		setTimeout(function() {
			el.classList.remove('fadein');
			el.classList.add('fadeout');
		}, 3000);
		setTimeout(function() {
			var data = document.querySelector(".data");
			data.parentNode.removeChild(data);
		}, 3500);
	}
	
	function getXSRF() {
		var meta = document.getElementsByName("_xsrf")[0];
		return meta.getAttribute('content');
	}
	
	var openDialog = function() {
		setDialog("open", {
			title: "Управление",
			width: 270,
			height: 400,
			content: "<button id='btn-graph' type='button' class='btn btn-default btn-lg'><span class='glyphicon glyphicon-indent-left' aria-hidden='true'></span> График</button><input type='checkbox' id='btn-dup' data-on='Дубликаты' data-off='Дубликаты' data-onstyle='success' data-offstyle='danger'><button id='btn-rem' type='button' class='btn btn-default btn-lg'><span class='glyphicon glyphicon-remove-circle' aria-hidden='true'></span> Очистить</button>"
		});
		$('#btn-dup').bootstrapToggle();
	}
	openDialog();
	
	//Объект графика
	var p1;
	//Объект с текущими свойствами графика
	var graphObject = {};
	
	function updateGraph() {
		var names = ["tblname","eventtype","start","end"];
		var sent_obj = {};
		names.forEach(function(id, i){
			var el = document.getElementById(id);
			if (i < 2) {
				var val = el.options[el.selectedIndex].value;
			} else {
				var val = el.value;
			}
			sent_obj[id] = val;
		});
		if (sent_obj["start"] == "") {
			sent_obj["start"] = "-3000";
		}
		if (sent_obj["end"] == "") {
			sent_obj["end"] = "2050";
		}
		reqwest({
			url: "http://{{.Url}}/appendtoflot",
			type: 'json',
			method: 'post',
			contentType: 'application/json; charset=utf-8',
			headers: {'X-Xsrftoken': getXSRF()},
			data: JSON.stringify(sent_obj),
			success: function(data){
				if (data != null) {
					var ticks = [];
					//Отделяем и преобразуем данные событий; определяем самую раннюю и позднюю даты
					var min = 2050;
					var max = -3000;
					var el = document.getElementById("eventtype");
					var event_type = el.options[el.selectedIndex].value;
					graphObject.data1 = [];
					var l = 0;
					var counter = 0;
					data.forEach(function(arr, i){
						if (arr != null && arr[0].from == undefined) {
							arr.forEach(function(item, j){
								if(item[0] < min){
									min = item[0];
								}
								if(item[2] > max){
									max = item[2];
								}
								ticks.push([counter, item[4]]);
								counter = counter + 1;
							});
							l = l + arr.length;
							graphObject.data1.push({ label: event_type, data: arr});
						}
					});
					
					//Отделяем данные дубликатов
					graphObject.lineData = [];
					data.forEach(function(obj, i){
						if (obj != null && obj[0].from != undefined) {
							for (var i=0; i<obj.length; i++) {
								graphObject.lineData.push(obj[i]);
							}
						}
					});

					graphObject.options = {
						legend:{         
							container:$("#legendcontainer"),            
							noColumns: 0
						},
						series: {
							editMode: 'v',
							editable:true,
							gantt: {
								active: true,
								show: true,
								barHeight: .5
							}
						},
						xaxis: {
							min: min-1,
							max: max+1,
							mode: null
						},
						yaxis: {
							min: 0,
							max: 10,
							ticks: ticks
						},
						grid: {
							hoverable: true,
							clickable: false,
							editable: true
						}
					};

					Draw();
				} else {
					complete("Событий не найдено!", ".alert-success");
				}
			},
			error: function() {
				complete("Ошибка!", ".alert-danger");
			}
		});
	}

	function showTooltip(x, y, contents){
		var div = document.createElement('div');
		document.body.appendChild(div);
		div.id = "tooltip";
		div.innerHTML = contents;
		div.style.top = String(y + 5)+"px";
		div.style.left = String(x + 5)+"px";
		div.classList.add('fadeinTooltip');
	}

	var previousPoint = null;
	$("#touchflot").bind("plothover", function (event, pos, item){	
		if(item){
			if (previousPoint != item.datapoint){
				previousPoint = item.datapoint;
				$("#tooltip").remove();
				if (item.series.data[item.dataIndex] != undefined) {
					showTooltip(pos.pageX, pos.pageY,
					"" + item.series.data[item.dataIndex][3] + "; " + item.series.data[item.dataIndex][0] +","+ item.series.data[item.dataIndex][2] + "");
				}
			}
		}
		else{
			$("#tooltip").remove();
			previousPoint = null;
		}
	});

	$("#touchflot").bind("datadrop", function(event, pos, item){
		var dI, data, fromLabel;
		if(item.dataIndex.length) {
			dI = item.dataIndex[0]
		} else {
			dI = item.dataIndex
		};
		data = item.series.data[dI];
		fromLabel = item.series.label;
		if(item.dataIndex.length){
			if(item.dataIndex[1] == 1){
				var dateFrom = data[0], dateTo = parseInt(pos.x1), idx = data[3], edge = "Начало ";
			}
			else if (item.dataIndex[1] == 2){
				var dateFrom = data[2], dateTo = parseInt(pos.x1), idx = data[3], edge = "Окончание ";
			}
			var el = document.getElementById("tblname");
			var tbl_name = el.options[el.selectedIndex].value;
			var obj = JSON.stringify({"tbl_name": tbl_name, "idx": idx, "dateto": dateTo, "edge": item.dataIndex[1]});
			reqwest({
				url: "http://{{.Url}}/eventchange",
				type: 'json',
				method: 'post',
				contentType: 'application/json; charset=utf-8',
				headers: {'X-Xsrftoken': getXSRF()},
				data: (obj),
				success: function() {
					var data = edge + idx + " перемещено с " + dateFrom + " на " + dateTo;
					complete(data, ".alert-success");
					updateGraph();
                    //Для обучения
                    var tf = document.getElementById('touchflot');
                    if (tf.classList.contains('not_changed')){
                        tf.classList.remove("not_changed");
                    }
				},
				error: function () {
					complete("Ошибка!", ".alert-danger");
				}
			});
		}
	});
		
	$('.datetimepicker').datetimepicker({
		viewMode: 'years',
		format: 'YYYY'
	});
	
	//Отобразить график
	function Draw() {
		p1 = $.plot("#touchflot", graphObject.data1, graphObject.options);
	}
	
	//Обновить график
	var graph = document.getElementById('btn-graph');
	graph.onclick = function() {
		updateGraph();
	}
	
	//Отобразить дубликаты
	var dup = document.querySelector(".toggle");
	dup.onclick = function() {
		if (dup.classList.contains("off")) {
			$.plot.JUMlib.drawing.drawLines(p1, graphObject.lineData);
		} else {
			Draw();
		}
	}
	
	//Очистить график
	var rem = document.getElementById('btn-rem');
	rem.onclick = function() {
		var tf = document.getElementById("touchflot");
		var lc = document.getElementById("legendcontainer");
		tf.innerHTML = "";
		lc.innerHTML = "";
	}
});
</script>