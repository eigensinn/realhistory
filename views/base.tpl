<!DOCTYPE html>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="_xsrf" content="{{.xsrf_token}}">
		<link href="/static/img/favicon.png" type="image/png" rel="icon">
		<link href="/static/css/bootstrap.min.css" rel="stylesheet">
		<link href="/static/css/base.css" rel="stylesheet">
		<link href="/static/css/dialogbox.css" rel="stylesheet">
		<link href="/static/fonts/sideshow-fontface.min.css" rel="stylesheet">
		<script src="/static/js/jquery-2.2.4.min.js"></script>
		<script src="/static/js/bootstrap.min.js"></script>
		<script src="/static/js/alerts.js"></script>
		<script src="/static/js/reqwest.min.js"></script>
		
		<link href="/static/css/sideshow.min.css" rel="stylesheet">
		<script src="/static/js/pagedown.min.js"></script>
		<script src="/static/js/jazz.min.js"></script>
		<script src="/static/js/sideshow.min.js"></script>
		<script src="/static/js/tutorial.js"></script>
		<script src="/static/js/dialogbox.js"></script>
		<script>
		document.addEventListener('DOMContentLoaded', function() {
			//Выделяет цветом ссылку в меню
			var act = document.querySelector({{.active}});
			act.classList.add("active");
			
			//Функция alert-сообщений
			function complete(data, cls) {
				var el = document.querySelector(cls);
				var span = document.createElement('span');
				el.appendChild(span);
				span.setAttribute("class", "data");
				span.innerHTML = data;
				el.classList.remove('fadeout');
				el.classList.add('fadein');
				setTimeout(function() {
					el.classList.remove('fadein');
					el.classList.add('fadeout');
				}, 3000);
				setTimeout(function() {
					var data = document.querySelector(".data");
					data.parentNode.removeChild(data);
				}, 3500);
			}
			
			function getXSRF() {
				var meta = document.getElementsByName("_xsrf")[0];
				return meta.getAttribute('content');
			}
			
			var logout = document.getElementById("logout");
			logout.onclick = function() {
				reqwest({
					url: "http://{{.Url}}/logout", method: 'get',
					headers: {'X-Xsrftoken': getXSRF()},
					error: function (err) { complete(err, ".alert-danger"); },
					success: function () {
						window.location.href = "/";
					}
				});
			}
			
			function validateEmail(email) {
				var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				return re.test(email);
			}
			
			function errorMessage(data) {
				var resp = document.createElement('span');
				resp.innerHTML = data;
				resp.classList.add("text-danger");
				var activetab = document.getElementById("message");
				activetab.appendChild(resp);
				activetab.classList.remove("fadeout");
				activetab.classList.add("fadein");
				setTimeout(function() {
					activetab.classList.remove('fadein');
					activetab.classList.add('fadeout');
				}, 3000);
				setTimeout(function() {
					activetab.removeChild(resp);
				}, 3500);
			}
			
			//Запрос при нажатии на Ок в форме входа
			var allow = document.getElementById("allow");
			allow.onclick = function() {
				var url, email, password;
				var auth_tab = document.getElementById("auth_tab");
				if (auth_tab.classList.contains("active")) {
					url = "http://{{.Url}}/auth";
					email = document.getElementById("email_auth").value;
					password = document.getElementById("pass_auth").value;
				} else {
					url = "http://{{.Url}}/createuser";
					email = document.getElementById("email_reg").value;
					password = document.getElementById("pass_reg").value;
				}
				if (!validateEmail(email)) {
					errorMessage("email некорректен");
				} else {
					var loader = document.getElementById("loader");
					loader.classList.remove("hide");
					reqwest({
						url: url, type: 'json', method: 'post', contentType: 'application/json; charset=utf-8',
						headers: {'X-Xsrftoken': getXSRF()},
						data: JSON.stringify({"email": email, "password": password}),
						error: function (err) { complete(err, ".alert-danger"); },
						success: function (data) {
							if(data.code == 1) {
								complete(data.text, ".alert-success");
								$('#login_mod').modal('hide');
								if (auth_tab.classList.contains("active") == true) {
									window.location.href = "/";
								}
							} else {
								errorMessage(data.text);
							}
						},
						complete: function () {
							loader.classList.add("hide");
						}
					});
				}
			}
			//Активируем вкладки
			var tabs = document.querySelectorAll("#auth_tabs a");
			Array.prototype.forEach.call(tabs, function(tab, i){
				tab.addEventListener('click', function(e){
					e.preventDefault();
					$(this).tab('show');
				});
			});
			//Password fields
			var pass = document.querySelectorAll(".pass");
			Array.prototype.forEach.call(pass, function(inp, i){
				inp.addEventListener("focus", function(){
					inp.type = 'text';
				});
				inp.addEventListener("blur", function(){
					inp.type = 'password';
				});
			});
			//Всплывающие подсказки
			$('[data-toggle="tooltip"]').tooltip()
			{{if .confirm}}
				complete({{.confirm}}, ".alert-success");
			{{end}}
		});
		</script>
		{{.Head}}
	</head>
	<body>
		<div class="container-fluid">
			{{.Header}}
			{{.Nav}}
			{{.LayoutContent}}
			{{.Footer}}
		</div>
		<div class="modal fade" id="login_mod" tabindex="-1" role="dialog" aria-labelledby="login_mod_lbl">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="rem_evnt" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="login_mod_lbl">Вход</h4>
					</div>
					<div class="modal-body row">
						<ul class="nav nav-pills nav-stacked col-md-4 col-lg-4" role="tablist" id="auth_tabs">
							<li role="presentation" id="auth_tab" class="active"><a href="#auth" class="pill">Авторизация</a></li>
							<li role="presentation" id="reg_tab"><a href="#reg" class="pill">Регистрация</a></li>
						</ul>
						<div class="tab-content col-md-8 col-lg-8">
							<div role="tabpanel" class="tab-pane active row" id="auth">
								<div class="col-md-6 col-lg-6 text-right">email</div><div class="col-md-6 col-lg-6"><input id="email_auth" type="text" value=""></div>
								<div class="col-md-6 col-lg-6 text-right">Пароль</div><div class="col-md-6 col-lg-6"><input id="pass_auth" class="pass" type="password" value=""></div>
							</div>
							<div role="tabpanel" class="tab-pane row" id="reg">
								<div class="col-md-6 col-lg-6 text-right">email</div><div class="col-md-6 col-lg-6"><input id="email_reg" type="text" value=""></div>
								<div class="col-md-6 col-lg-6 text-right">Пароль</div><div class="col-md-6 col-lg-6"><input id="pass_reg" class="pass" type="password" value=""></div>
							</div>
							<div class="col-md-12 col-lg-12 fadeout" id="message"></div>
							<div class="col-md-12 col-lg-12 text-center row hide" id="loader">
								<div class="stick" id="stick1"></div>
								<div class="stick" id="stick2"></div>
								<div class="stick" id="stick3"></div>
								<div class="stick" id="stick4"></div>
								<div class="stick" id="stick5"></div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
						<button type="button" id="allow" class="btn btn-primary">ОК</button>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="logout_mod" tabindex="-1" role="dialog" aria-labelledby="logout_mod_lbl">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="rem_evnt" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="logout_mod_lbl">Выход</h4>
					</div>
					<div class="modal-body">
						<p>Вы действительно хотите выйти?</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
						<button type="button" id="logout" class="btn btn-primary" data-dismiss="modal">ОК</button>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>