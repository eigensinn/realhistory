<header>
	<div class="row">
		<div class="row col-lg-12 col-md-12">
			<div class="logo_wrap col-lg-offset-4 col-md-offset-4 col-lg-4 col-md-4">
				<a id="lnk_home" href="/"><img src="/static/img/logo.png"></a>
			</div>
			<div class="row col-lg-4 col-md-4">
				<div class="col-lg-12 col-md-12">
					<a class="a_gray" href="#">
                        <span data-toggle="tooltip" data-placement="left" title="Настройки">
							<span id="lnk_settings" class="lnk glyphicon glyphicon-cog"></span>
						</span>
                    </a>
				</div>
				<div class="col-lg-12 col-md-12">
					<a class="a_gray" href="#">
						{{if .email}}
						<span class="lnk" data-toggle="tooltip" data-placement="left" title="Выход">
							<span data-toggle="modal" data-target="#logout_mod">{{.email}}</span>
						</span>
						{{else}}
						<span data-toggle="tooltip" data-placement="left" title="Вход">
							<span id="lnk_login" class="lnk glyphicon glyphicon-user" data-toggle="modal" data-target="#login_mod"></span>
						</span>
						{{end}}
					</a>
				</div>
			</div>
		</div>
		<div class="row lbl_header col-lg-12 col-md-12">
			<div class="col-lg-offset-3 col-md-offset-3 col-lg-6 col-md-6">
				<p class="text-center">Реальная история</p>
			</div>
		</div>
	</div>
</header>
<div class="modal fade" id="settings_mod" tabindex="-1" role="dialog" aria-labelledby="settings_mod_lbl">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="rem_evnt" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="settings_mod_lbl">Настройки</h4>
            </div>
            <div class="modal-body">
                <form id="settings">
                    <div class="row col-lg-4 col-md-4">
                        <div class="input-group">
                            <input name="host" type="text" class="form-control" placeholder="Host" aria-describedby="basic-addon0">
                        </div>
                        <div class="input-group">
                            <input name="db_user" type="text" class="form-control" placeholder="DbUser" aria-describedby="basic-addon1">
                        </div>
                    </div>
                    <div class="row col-lg-4 col-md-4">
                        <div class="input-group">
                            <input name="db_name" type="text" class="form-control" placeholder="DbName" aria-describedby="basic-addon2">
                        </div>
                        <div class="input-group">
                            <input name="ssl_mode" type="text" class="form-control" placeholder="SslMode" aria-describedby="basic-addon3">
                        </div>
                    </div>
                    <div class="row col-lg-4 col-md-4">
                        <div class="input-group">
                            <input name="db_pwd" type="text" class="form-control" placeholder="DbPwd" aria-describedby="basic-addon4">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-md-12 col-lg-12 text-center row hide" id="s_loader">
                    <div class="stick" id="s_stick1"></div>
                    <div class="stick" id="s_stick2"></div>
                    <div class="stick" id="s_stick3"></div>
                    <div class="stick" id="s_stick4"></div>
                    <div class="stick" id="s_stick5"></div>
                </div>
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" id="save_settings" class="btn btn-primary">ОК</button>
            </div>
        </div>
    </div>
</div>
<script>
document.addEventListener('DOMContentLoaded', function() {
    function complete(data, cls) {
		var el = document.querySelector(cls);
		var span = document.createElement('span');
		el.appendChild(span);
		span.setAttribute("class", "data");
		span.innerHTML = data;
		el.classList.remove('fadeout');
		el.classList.add('fadein');
		setTimeout(function() {
			el.classList.remove('fadein');
			el.classList.add('fadeout');
		}, 3000);
		setTimeout(function() {
			var data = document.querySelector(".data");
			data.parentNode.removeChild(data);
		}, 5000);
	}

    //Функция сериализации формы
	$.fn.serializeFormJSON = function() {
	   var o = {};
	   var a = this.serializeArray();
	   a.forEach(function(el, i){
		   if (o[el.name]) {
			   if (!o[el.name].push) {
				   o[el.name] = [o[el.name]];
			   }
			   o[el.name].push(el.value || '');
		   } else {
			   o[el.name] = el.value || '';
		   }
	   });
	   return o;
	};

    function getXSRF() {
		var meta = document.getElementsByName("_xsrf")[0];
		return meta.getAttribute('content');
	}
    
    var show_settings = document.getElementById('lnk_settings');
	show_settings.onclick = function() {
		$('#settings_mod').modal('show');
	}
    
    //Сохранение настроек
	var save_settings = document.getElementById('save_settings');
	save_settings.onclick = function() {
        var loader = document.getElementById("s_loader");
		loader.classList.remove("hide");
		reqwest({
			url: "http://{{.Url}}/setsettings",
			type: 'json',
			method: 'post',
			contentType: 'application/json; charset=utf-8',
			headers: {'X-Xsrftoken': getXSRF()},
			data: JSON.stringify($('#settings').serializeFormJSON()),
			error: function (err) { complete(err, ".alert-danger"); },
			success: function (data) {
                loader.classList.add("hide");
                $('#settings_mod').modal('hide');
				complete(data, ".alert-success");
			}
		});
	}
});
</script>